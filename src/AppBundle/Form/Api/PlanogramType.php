<?php

namespace AppBundle\Form\Api;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use AppBundle\Entity\KeyVisual;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\PlanogramCigarette;
use AppBundle\Entity\Showzone;
use AppBundle\Entity\Sort;
use AppBundle\Entity\Stripe;
use AppBundle\Entity\Vendor;
use AppBundle\Form\DataTransformer\FileToBase64Transformer;
use AppBundle\Form\PlanogramCigaretteType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PlanogramType
 *
 * @package AppBundle\Form\Api
 */
class PlanogramType extends AbstractType
{
    private $fileToBase64Transformer;

    public function __construct(
        FileToBase64Transformer $fileToBase64Transformer
    )
    {
        $this->fileToBase64Transformer = $fileToBase64Transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            //->add('name', null)
            ->add('type', ChoiceType::class, array(
                'choices' => Planogram::$TYPES
            ))
            ->add('showzoneType', ChoiceType::class, array(
                'choices' => Planogram::$SHOWZONE_TYPES,
                'required' => false
            ))
            ->add('keyVisual', EntityType::class, array(
                'class' => KeyVisual::class
            ))
            ->add('brand', EntityType::class, array(
                'class' => Brand::class
            ))
            ->add('vendor', EntityType::class, array(
                'class' => Vendor::class
            ))
            ->add('stripe', EntityType::class, array(
                'class' => Stripe::class
            ))
            ->add('showzone', EntityType::class, array(
                'class' => Showzone::class
            ))
            ->add('sort', EntityType::class, array(
                'class' => Sort::class
            ))
            ->add('cigarettes', CollectionType::class, array(
                'by_reference' => false, // Use this because of reasons
                'allow_add' => true, // True if you want allow adding new entries to the collection
                'allow_delete' => true, // True if you want to allow deleting entries
                'prototype' => true, // True if you want to use a custom form type
                'entry_type' => PlanogramCigaretteType::class,
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Planogram::class
        ));
    }

    //get rid of class name prefix when referring to form fields
    public function getBlockPrefix()
    {
        return '';
    }
}