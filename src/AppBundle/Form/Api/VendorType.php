<?php

namespace AppBundle\Form\Api;


use AppBundle\Entity\Vendor;
use AppBundle\Form\DataTransformer\FileToBase64Transformer;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ShowzoneType
 *
 * @package AppBundle\Form\Api
 */
class VendorType extends AbstractType
{
    private $fileToBase64Transformer;

    public function __construct(
        FileToBase64Transformer $fileToBase64Transformer
    )
    {
        $this->fileToBase64Transformer = $fileToBase64Transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null)
            ->add('width', null)
            ->add('height', null)
            ->add('cols', null)
            ->add('rows', null)
            ->add('stripeHeight', null)
            ->add('rowsHeight', null)
            ->add('visualPosLeft', null)
            ->add('visualPosTop', null)
            ->add('visualWidth', null)
            ->add('visualHeight', null)
            ->add('innerPosLeft', null)
            ->add('innerPosTop', null)
            ->add('innerWidth', null)
            ->add('innerHeight', null)
            ->add('headerHeight', null)
            ->add('imageFile', TextType::class, array(
                'invalid_message' => 'This is not a file!',
            ))
        ;

        $builder->get('imageFile')
            ->addModelTransformer($this->fileToBase64Transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Vendor::class
        ));
    }

    //get rid of class name prefix when referring to form fields
    public function getBlockPrefix()
    {
        return '';
    }
}