<?php

namespace AppBundle\Form\Api;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\PlanogramCigarette;
use AppBundle\Entity\Showzone;
use AppBundle\Form\DataTransformer\FileToBase64Transformer;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class ShowzoneType
 *
 * @package AppBundle\Form\Api
 */
class ShowzoneType extends AbstractType
{
    private $fileToBase64Transformer;

    public function __construct(
        FileToBase64Transformer $fileToBase64Transformer
    )
    {
        $this->fileToBase64Transformer = $fileToBase64Transformer;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null)
            ->add('type', ChoiceType::class, array(
                'choices' => Showzone::$TYPES
            ))
            ->add('imageFile', TextType::class, array(
                // validation message if the data transformer fails
                'invalid_message' => 'This is not a file!',
            ))
        ;

        $builder->get('imageFile')
            ->addModelTransformer($this->fileToBase64Transformer);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Showzone::class
        ));
    }

    //get rid of class name prefix when referring to form fields
    public function getBlockPrefix()
    {
        return '';
    }
}