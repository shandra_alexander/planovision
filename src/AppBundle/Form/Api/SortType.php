<?php

namespace AppBundle\Form\Api;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use AppBundle\Entity\KeyVisual;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\PlanogramCigarette;
use AppBundle\Entity\Showzone;
use AppBundle\Entity\Stripe;
use AppBundle\Form\DataTransformer\FileToBase64Transformer;
use Imagine\Filter\Basic\Strip;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class SortType
 *
 * @package AppBundle\Form\Api
 */
class SortType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Sort::class
        ));
    }

    public function getBlockPrefix()
    {
        return '';
    }
}