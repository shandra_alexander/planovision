<?php

namespace AppBundle\Form;

use AppBundle\Entity\Cigarette;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\PlanogramCigarette;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;

/**
 * Class PlanogramCigaretteType
 *
 * @package AppBundle\Form
 */
class PlanogramCigaretteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('cigarette', EntityType::class, array(
                'label' => 'Сигареты',
                'class' => Cigarette::class,
            ))
            ->add('col', NumberType::class, array(
                'label' => 'Колонка'
            ))
            ->add('row', NumberType::class, array(
                'label' => 'Ряд'
            ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => PlanogramCigarette::class
        ));
    }

    //get rid of class name prefix when referring to form fields
    public function getBlockPrefix()
    {
        return '';
    }
}