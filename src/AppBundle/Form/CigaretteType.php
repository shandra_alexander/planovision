<?php

namespace AppBundle\Form;

use AppBundle\Entity\Cigarette;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\PlanogramCigarette;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\FormView;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\Form\FormBuilderInterface;
use Vich\UploaderBundle\Form\Type\VichImageType;

/**
 * Class CigaretteType
 *
 * @package AppBundle\Form
 */
class CigaretteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', TextType::class, array(
                'label' => 'Название'
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Тип',
                'choices' => Cigarette::$TYPES
            ))
            ->add('imageFile', VichImageType::class, [
                'label' => 'Изображение пачки',
                'required' => false,
                'allow_delete' => false
            ]);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => Cigarette::class
        ));
    }

    //get rid of class name prefix when referring to form fields
    public function getBlockPrefix()
    {
        return '';
    }
}