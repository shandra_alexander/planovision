<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Form\DataTransformer;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\DataTransformerInterface;
use Symfony\Component\Form\Exception\TransformationFailedException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Serializer\Normalizer\DataUriNormalizer;

class FileToBase64Transformer implements DataTransformerInterface
{

    private $entityManager;
    private $normalizer;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
        $this->normalizer = new DataUriNormalizer();
    }

    /**
     * Transforms an object (file) to a Base64 string.
     *
     * @param  File|UploadedFile|null $file
     * @return string
     */
    public function transform($file)
    {
        if (null === $file) {
            return "";
        }

        if ($this->normalizer->supportsNormalization($file)) {
            $string = $this->normalizer->normalize($file);
        } else {
            throw new TransformationFailedException(sprintf(
                'Couldn\'t transform file into Base64!'
            ));
        }

        return $string;
    }

    /**
     * Transforms a Base64 string to an object (file).
     *
     * @param  string $string
     *
     * @return File|UploadedFile|null
     *
     * @throws TransformationFailedException if object (tree) is not found.
     */
    public function reverseTransform($string)
    {
        if (!$string) {
            return null;
        }
        //This solution was found here: https://bit.ly/2ze4uvz 
        //create a temp name
        $tmpFilePath = tempnam(sys_get_temp_dir(), 'photo_');
        //Open for reading and writing; 
        //place the file pointer at the beginning of the file 
        //and truncate the file to zero length. 
        //If the file does not exist, attempt to create it.
        $tmp = fopen($tmpFilePath, 'wb+');
        $matches = [];
        //extract type and data from the string
        preg_match('/^data:([\w-]+\/[\w-]+);base64,(.+)$/', $string, $matches);
        
        //check if the string is a proper base64 string
        if (base64_decode($matches[2], true)) {
            //write into a file. Store its size
            $size = fwrite($tmp, base64_decode($matches[2]));
        } else {
            throw new TransformationFailedException(sprintf(
                'Couldn\'t transform "%s" into an object!', $string
            ));
        }

        //close the stream
        fclose($tmp);

        //create an instance of UploadedFile. 
        //The issue with this described in here: https://bit.ly/2DkMYIR
        $file = new UploadedFile($tmpFilePath, 'photoFile', $matches[1], $size, 0, true);

        if (null === $file) {
            throw new TransformationFailedException(sprintf(
                'Couldn\'t create an instance of UploadedFile object!', $string
            ));
        }

        return $file;
    }
}
