<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use AppBundle\Entity\Brand;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as StdCollectionType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class KeyVisualAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('title', TextType::class, array(
                'label' => 'Название'
            ))
            ->add('widthRatio', NumberType::class, array(
                'label' => 'Ширина'
            ))
            ->add('heightRatio', NumberType::class, array(
                'label' => 'Высота'
            ))
            ->add('brand', EntityType::class, array(
                'label' => 'Бренд',
                'class' => Brand::class,
            ))
            ->add('imageFile', VichImageType::class, [
                'label' => 'Изображение',
                'required' => false,
                'allow_delete' => true
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title', null, array('label' => 'Название'));
        $datagridMapper->add('widthRatio', null, array('label' => 'Ширина'));
        $datagridMapper->add('heightRatio', null, array('label' => 'Высота'));
        $datagridMapper->add('brand', null, array('label' => 'Бренд'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //configure fields that are shown in 
        //the list of DB entries on SonataAdmin
        $listMapper->addIdentifier('title', null, array('label' => 'Название'));
        $listMapper->addIdentifier('widthRatio', null, array('label' => 'Ширина'));
        $listMapper->addIdentifier('heightRatio', null, array('label' => 'Высота'));
        $listMapper->add('brand', null, array('label' => 'Бренд'));
    }
}
