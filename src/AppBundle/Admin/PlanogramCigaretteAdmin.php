<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use AppBundle\Entity\BaseUser;
use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use AppBundle\Entity\KeyVisual;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\Showzone;
use AppBundle\Entity\Sort;
use AppBundle\Entity\Stripe;
use AppBundle\Entity\Vendor;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as StdCollectionType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class PlanogramCigaretteAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('planogram',EntityType::class, array(
            'label' => 'Планограма',
            'class' => Planogram::class,
        ));
        $formMapper->add('cigarette',EntityType::class, array(
            'label' => 'Сигареты',
            'class' => Cigarette::class,
        ));
        $formMapper->add('col',null, array(
            'label' => 'Колонка'
        ));
        $formMapper->add('row',null, array(
            'label' => 'Ряд'
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('planogram', null, array('label' => 'Планограма'));
        $datagridMapper->add('cigarette', null, array('label' => 'Сигареты'));
        $datagridMapper->add('col',null, array('label' => 'Колонка'));
        $datagridMapper->add('row',null, array('label' => 'Ряд'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('planogram', null, array('label' => 'Планограма'));
        $listMapper->add('cigarette', null, array('label' => 'Сигареты'));
        $listMapper->add('col',null, array('label' => 'Колонка'));
        $listMapper->add('row',null, array('label' => 'Ряд'));
    }
}
