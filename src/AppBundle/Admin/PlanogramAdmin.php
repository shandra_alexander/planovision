<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use AppBundle\Entity\BaseUser;
use AppBundle\Entity\Brand;
use AppBundle\Entity\KeyVisual;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\Showzone;
use AppBundle\Entity\Sort;
use AppBundle\Entity\Stripe;
use AppBundle\Entity\Vendor;
use AppBundle\Form\PlanogramCigaretteType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as StdCollectionType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class PlanogramAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('type', ChoiceType::class, array(
            'label' => 'Тип',
            'choices' => Planogram::$TYPES
        ));
        $formMapper->add('showzoneType', ChoiceType::class, array(
            'label' => 'Тип шоузоны',
            'required' => false,
            'choices' => Planogram::$SHOWZONE_TYPES
        ));
        $formMapper->add('user',EntityType::class, array(
            'label' => 'Пользователь',
            'class' => BaseUser::class,
        ));
        $formMapper->add('keyVisual',EntityType::class, array(
            'label' => 'KeyVisual',
            'required' => false,
            'class' => KeyVisual::class,
        ));
        $formMapper->add('brand',EntityType::class, array(
            'label' => 'Бренд',
            'class' => Brand::class,
        ));
        $formMapper->add('vendor',EntityType::class, array(
            'label' => 'Вендор',
            'class' => Vendor::class,
        ));
        $formMapper->add('stripe',EntityType::class, array(
            'label' => 'Полоса',
            'required' => false,
            'class' => Stripe::class,
        ));
        $formMapper->add('showzone',EntityType::class, array(
            'label' => 'Шоузона',
            'required' => false,
            'class' => Showzone::class,
        ));
        $formMapper->add('sort',EntityType::class, array(
            'label' => 'Сортировка',
            'required' => false,
            'class' => Sort::class,
        ));
        $formMapper->add('createdAt', null, array( 'label' => 'Дата создания'));
        $formMapper->add('updatedAt', null, array( 'label' => 'Дата обновления'));
        $formMapper->add('cigarettes', StdCollectionType::class, [
            'by_reference' => false, // Use this because of reasons
            'allow_add' => true, // True if you want allow adding new entries to the collection
            'allow_delete' => true, // True if you want to allow deleting entries
            'prototype' => true, // True if you want to use a custom form type
            'entry_type' => PlanogramCigaretteType::class, // Form type for the Entity that is being attached to the object
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('type', null, array('label' => 'Тип'));
        $datagridMapper->add('showzoneType', null, array('label' => 'Тип шоузоны'));
        $datagridMapper->add('user',null, array('label' => 'Пользователь'));
        $datagridMapper->add('keyVisual',null, array('label' => 'KeyVisual'));
        $datagridMapper->add('brand',null, array('label' => 'Бренд'));
        $datagridMapper->add('vendor',null, array('label' => 'Вендор'));
        $datagridMapper->add('stripe',null, array('label' => 'Полоса'));
        $datagridMapper->add('showzone',null, array( 'label' => 'Шоузона'));
        $datagridMapper->add('sort',null, array('label' => 'Сортировка'));
        $datagridMapper->add('createdAt', null, array( 'label' => 'Дата создания'));
        $datagridMapper->add('updatedAt', null, array( 'label' => 'Дата обновления'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('type', null, array('label' => 'Тип'));
        $listMapper->add('showzoneType', null, array('label' => 'Тип шоузоны'));
        $listMapper->add('user',null, array('label' => 'Пользователь'));
        $listMapper->add('keyVisual',null, array('label' => 'KeyVisual'));
        $listMapper->add('brand',null, array('label' => 'Бренд'));
        $listMapper->add('vendor',null, array('label' => 'Вендор'));
        $listMapper->add('stripe',null, array('label' => 'Полоса'));
        $listMapper->add('showzone',null, array( 'label' => 'Шоузона'));
        $listMapper->add('sort',null, array('label' => 'Сортировка'));
        $listMapper->add('createdAt', null, array( 'label' => 'Дата создания'));
        $listMapper->add('updatedAt', null, array( 'label' => 'Дата обновления'));
    }
}
