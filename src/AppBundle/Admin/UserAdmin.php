<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use AppBundle\Entity\Country;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use AppBundle\Entity\Chat;
use AppBundle\Entity\BaseActivity;
use Symfony\Component\Security\Core\Encoder\EncoderFactory;
use Symfony\Component\Security\Core\Encoder\EncoderFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Description of UserAdmin
 *
 * @author eugene
 * 
 * UserAdmin Class for SonataAdmin.
 * Uses User Entity. Acts as a representational
 * model for displaying, filtering, and editing
 * of the entries in MySQL DB through Doctrine ORM.
 */
class UserAdmin extends AbstractAdmin
{

    protected function configureFormFields(FormMapper $formMapper)
    {
        //set PlainPassword a not null value due to @Assert\NotBlank()
        //$this->getSubject()->setPlainPassword('1');

        //configure form fields that are used whenever 
        //you are creating an entry or editing existing one
        //FORMAT: (fieldName, FieldType, ArrayWithExtraParams)
        $formMapper->add('firstName', TextType::class, array(
            'label' => 'Имя'
        ));
        $formMapper->add('lastName', TextType::class, array(
            'label' => 'Фамилия'
        ));
        $formMapper->add('login', TextType::class, array(
            'label' => 'Логин'
        ));
        $formMapper->add('plainPassword', PasswordType::class, array( //gotta test that
            'label' => 'Пароль'
        ));
        $formMapper->add('role', ChoiceType::class, array(
            'label' => 'Роль',
            'choices' => array(
                'Администратор' => 'ROLE_ADMIN',
                'Пользователь' => 'ROLE_USER'
            )
        ));
        $formMapper->add('isActive', ChoiceType::class, array(
            'label' => 'Активность',
            'choices' => array(
                'Активен' => true,
                'Не Активен' => false
            )
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        //configure filters that are used 
        //to filter entries in the main listing page
        $datagridMapper->add('firstName', null, array('label' => 'Имя'));
        $datagridMapper->add('lastName', null, array('label' => 'Фамилия'));
        $datagridMapper->add('login', null, array('label' => 'Логин'));
        $datagridMapper->add('isActive', null, array(
            'label' => 'Активность',
            'choices' => array(
                'Активен' => true,
                'Не Активен' => false
            )
        ));
        //$datagridMapper->add('dateCreated', null, array('label' => 'Дата Создания'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //configure fields that are shown in 
        //the list of DB entries on SonataAdmin
        $listMapper->addIdentifier('firstName', null, array(
            'label' => 'Имя'
        ));
        $listMapper->addIdentifier('lastName', null, array(
            'label' => 'Фамилия'
        ));
        $listMapper->addIdentifier('login', null, array(
            'label' => 'Логин'
        ));
        $listMapper->addIdentifier('role', null, array(
            'label' => 'Роль'
        ));
        $listMapper->addIdentifier('dateCreated', null, array(
            'label' => 'Дата Создания'
        ));
    }

    public function preUpdate($user)
    {
        $this->encodePassword($user);
    }
    
    public function prePersist($user)
    {
        $this->encodePassword($user);
    }
    
    private function encodePassword($user)
    {
        $factory = $this->getConfigurationPool()->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($this->getForm()
            ->get('plainPassword')->getData(), $user->getSalt());
        $user->setPassword($password);
    }
}
