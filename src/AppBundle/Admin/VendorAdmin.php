<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as StdCollectionType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use AppBundle\Entity\Chat;
use Vich\UploaderBundle\Form\Type\VichImageType;

class VendorAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', null, array('label' => 'Название'));
        $formMapper->add('width', null, array( 'label' => 'ширина'));
        $formMapper->add('height', null, array( 'label' => 'высота'));
        $formMapper->add('cols', null, array( 'label' => 'кол-во столбцов'));
        $formMapper->add('rows', null, array( 'label' => 'кол-во рядов'));
        $formMapper->add('stripeHeight', null, array( 'label' => 'высота полосы'));
        $formMapper->add('rowsHeight', null, array( 'label' => 'высотя рядов'));
        $formMapper->add('visualPosLeft', null, array( 'label' => 'Позиция KVisual слева'));
        $formMapper->add('visualPosTop', null, array( 'label' => 'Позиция KVisual сверху'));
        $formMapper->add('visualWidth', null, array( 'label' => 'Ширина KVisual'));
        $formMapper->add('visualHeight', null, array( 'label' => 'Высота KVisual'));
        $formMapper->add('innerPosLeft', null, array( 'label' => 'Внутрення позиция слева'));
        $formMapper->add('innerPosTop', null, array( 'label' => 'Внутрення позиция сверху'));
        $formMapper->add('innerWidth', null, array( 'label' => 'Внутрення ширина'));
        $formMapper->add('innerHeight', null, array( 'label' => 'Внутрення высота'));
        $formMapper->add('imageFile', VichImageType::class, [
            'label' => 'Изображение',
            'required' => true,
            'allow_delete' => false
        ]);
        $formMapper->add('createdAt', null, array( 'label' => 'Дата создания'));
        $formMapper->add('updatedAt', null, array( 'label' => 'Дата обновления'));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, array('label' => 'Название'));
        $datagridMapper->add('width', null, array( 'label' => 'ширина'));
        $datagridMapper->add('height', null, array( 'label' => 'высота'));
        $datagridMapper->add('cols', null, array( 'label' => 'кол-во столбцов'));
        $datagridMapper->add('rows', null, array( 'label' => 'кол-во рядов'));
        $datagridMapper->add('stripeHeight', null, array( 'label' => 'высота полосы'));
        $datagridMapper->add('rowsHeight', null, array( 'label' => 'высотя рядов'));
        $datagridMapper->add('visualPosLeft', null, array( 'label' => 'Позиция KVisual слева'));
        $datagridMapper->add('visualPosTop', null, array( 'label' => 'Позиция KVisual сверху'));
        $datagridMapper->add('visualWidth', null, array( 'label' => 'Ширина KVisual'));
        $datagridMapper->add('visualHeight', null, array( 'label' => 'Высота KVisual'));
        $datagridMapper->add('innerPosLeft', null, array( 'label' => 'Внутрення позиция слева'));
        $datagridMapper->add('innerPosTop', null, array( 'label' => 'Внутрення позиция сверху'));
        $datagridMapper->add('innerWidth', null, array( 'label' => 'Внутрення ширина'));
        $datagridMapper->add('innerHeight', null, array( 'label' => 'Внутрення высота'));
        $datagridMapper->add('createdAt', null, array( 'label' => 'Дата создания'));
        $datagridMapper->add('updatedAt', null, array( 'label' => 'Дата обновления'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('name', null, array('label' => 'Название'));
        $listMapper->add('width', null, array( 'label' => 'ширина'));
        $listMapper->add('height', null, array( 'label' => 'высота'));
        $listMapper->add('cols', null, array( 'label' => 'кол-во столбцов'));
        $listMapper->add('rows', null, array( 'label' => 'кол-во рядов'));
        $listMapper->add('stripeHeight', null, array( 'label' => 'высота полосы'));
        $listMapper->add('rowsHeight', null, array( 'label' => 'высотя рядов'));
        $listMapper->add('visualPosLeft', null, array( 'label' => 'Позиция KVisual слева'));
        $listMapper->add('visualPosTop', null, array( 'label' => 'Позиция KVisual сверху'));
        $listMapper->add('visualWidth', null, array( 'label' => 'Ширина KVisual'));
        $listMapper->add('visualHeight', null, array( 'label' => 'Высота KVisual'));
        $listMapper->add('innerPosLeft', null, array( 'label' => 'Внутрення позиция слева'));
        $listMapper->add('innerPosTop', null, array( 'label' => 'Внутрення позиция сверху'));
        $listMapper->add('innerWidth', null, array( 'label' => 'Внутрення ширина'));
        $listMapper->add('innerHeight', null, array( 'label' => 'Внутрення высота'));
        $listMapper->add('createdAt', null, array( 'label' => 'Дата создания'));
        $listMapper->add('updatedAt', null, array( 'label' => 'Дата обновления'));
    }
}
