<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as StdCollectionType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichImageType;

class CigaretteAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', TextType::class, array(
                'label' => 'Название'
            ))
            ->add('type', ChoiceType::class, array(
                'label' => 'Тип',
                'choices' => Cigarette::$TYPES
            ))
            ->add('price', NumberType::class, array(
                'label' => 'Цена'
            ))
            ->add('brand', EntityType::class, array(
                'label' => 'Бренд',
                'class' => Brand::class,
            ))
            ->add('imageFile', VichImageType::class, [
                'label' => 'Изображение пачки',
                'required' => false,
                'allow_delete' => false
            ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name', null, array('label' => 'Название'));
        $datagridMapper->add('type', null, array('label' => 'Тип'));
        $datagridMapper->add('price', null, array('label' => 'Цена'));
        $datagridMapper->add('brand', null, array('label' => 'Бренд'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //configure fields that are shown in 
        //the list of DB entries on SonataAdmin
        $listMapper->addIdentifier('name', null, array('label' => 'Название'));
        $listMapper->add('type', null, array('label' => 'Тип'));
        $listMapper->add('price', null, array('label' => 'Цена'));
        $listMapper->add('brand', null, array('label' => 'Бренд'));
    }
}
