<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType as StdCollectionType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Vich\UploaderBundle\Form\Type\VichFileType;
use AppBundle\Entity\Chat;

class AdminAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('login', TextType::class, array(
                'label' => 'Логин'
            ))
            ->add('plainPassword', PasswordType::class, array(
                'label' => 'Пароль'
            ))->add('role', ChoiceType::class, array(
                'label' => 'Роль',
                'choices' => array(
                    'Администратор' => 'ROLE_ADMIN',
                    'Пользователь' => 'ROLE_USER'
                )
            ))
            ->add('isActive', ChoiceType::class, array(
                'label' => 'Активность',
                'choices' => array(
                    'Активен' => true,
                    'Не Активен' => false
                )
            ));

        /*
        $formMapper->add('access', ChoiceType::class, array(
                'choices' => array(
                    'Магазины' => 'Shop',
                    'Пользователи' => 'User',
                    'Партнеры' => 'Partner',
                    'Посадки' => 'Planting',
                    'Акции' => 'Event',
                    'Новости' => 'Article'
                ),
                'expanded'  => true,
                'multiple'  => true,
                )
            );
        */
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('login', null, array('label' => 'Логин'));
        $datagridMapper->add('role', null, array('label' => 'Роль'));
        $datagridMapper->add('dateCreated', null, array('label' => 'Дата Создания'));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        //configure fields that are shown in 
        //the list of DB entries on SonataAdmin
        $listMapper->addIdentifier('login', null, array(
            'label' => 'Логин'
        ));
        $listMapper->addIdentifier('role', null, array(
            'label' => 'Роль'
        ));
        $listMapper->addIdentifier('dateCreated', null, array(
            'label' => 'Дата Создания'
        ));
        $listMapper->addIdentifier('access', null, array(
            'label' => 'Доступ'
        ));
    }
    
    public function preUpdate($user)
    {
        $this->encodePassword($user);
    }
    
    public function prePersist($user)
    {
        $this->encodePassword($user);
    }
    
    private function encodePassword($user)
    {
        $factory = $this->getConfigurationPool()->getContainer()->get('security.encoder_factory');
        $encoder = $factory->getEncoder($user);
        $password = $encoder->encodePassword($this->getForm()
            ->get('plainPassword')->getData(), $user->getSalt());
        $user->setPassword($password);
    }
}
