<?php
namespace AppBundle\DataFixtures;

use AppBundle\Entity\Cigarette;

class Data
{

    private $cigarettes = array(
        'Winston' => [
            [
                'name' => 'Winston Blue',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'Winston_blue_KS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Blue',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Winston_blue_SS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Dual',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Winston_dual_compact.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Impulse Compact',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Winston_impulse_compact.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Impulse SS 1',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Winston_impulse_SS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Impulse SS 2',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Winston_impulse_SS1.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Plus',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Winston_plus_compact.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Silver KS',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'Winston_silver_KS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Silver KSSS',
                'type' => Cigarette::TYPE_KING_SIZE_SUPER_SLIMS,
                'file_name' => 'Winston_silver_KSSS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston Silver SS',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Winston_silver_SS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston SummerMix',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Winston_summerMix_compact.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston XS KSSS',
                'type' => Cigarette::TYPE_KING_SIZE_SUPER_SLIMS,
                'file_name' => 'Winston_XS_KSSS.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston XStyle Blue',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Winston_xstyle_blue_compact.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ],
            [
                'name' => 'Winston XStyle Silver',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Winston_Xstyle_silver_compact.png',
                'file_path' => '/cigarettes_images/Winston_2px/'
            ]
        ],
        'LD' => [
            [
                'name' => 'LD Blue',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'LD_blue_KS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Red',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'LD_red_KS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Silver',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'LD_silver_KS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Club Bronze',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'LD_club compact_bronze.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Club Compact',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'LD_club_compact.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Club Compact Plus',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'LD_Club_compact plus.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Club Compact Blue',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'LD_Club_compact_blue.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Club Compact Silver',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'LD_club_compact_silver.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Impulse Compact',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'LD_Impulse_Compact.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Club Compact Super Slim',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'LD_club_compact_SS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Impulse',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'LD_impulse_SS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Pink',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'LD_pink_SS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
            [
                'name' => 'LD Violet',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'LD_violet_SS.png',
                'file_path' => '/cigarettes_images/LD_2px/'
            ],
        ],
        'Mevius'=> [
            [
                'name' => 'Mevius Option Revo',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Mevius Option_Revo_SS.png',
                'file_path' => '/cigarettes_images/Mevius_2px/'
            ],
            [
                'name' => 'Mevius Compact',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Mevius_compact.png',
                'file_path' => '/cigarettes_images/Mevius_2px/'
            ],
            [
                'name' => 'Mevius Revo Compact',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Mevius_revo_compact.png',
                'file_path' => '/cigarettes_images/Mevius_2px/'
            ],
            [
                'name' => 'Mevuis Wind Blue',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'Mevuis_wind blue_KS.png',
                'file_path' => '/cigarettes_images/Mevius_2px/'
            ],
        ],
        'Camel' => [
            [
                'name' => 'Camel Compact',
                'type' => Cigarette::TYPE_COMPACT,
                'file_name' => 'Camel_Compact.png',
                'file_path' => '/cigarettes_images/Camel_2px/'
            ],
            [
                'name' => 'Camel',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'Camel_KS.png',
                'file_path' => '/cigarettes_images/Camel_2px/'
            ],
        ],
        'Richmond' => [
            [
                'name' => 'Richmond Bronze',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Richmond_bronze_SS.png',
                'file_path' => '/cigarettes_images/Kiss_Richmond_2px/'
            ],
            [
                'name' => 'Richmond Red',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Richmond_red_SS.png',
                'file_path' => '/cigarettes_images/Kiss_Richmond_2px/'
            ],
        ],
        'Kiss' => [
            [
                'name' => 'Kiss Brown',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Kiss_brown_SS.png',
                'file_path' => '/cigarettes_images/Kiss_Richmond_2px/'
            ],
            [
                'name' => 'Kiss Jolly',
                'type' => Cigarette::TYPE_SUPER_SLIMS,
                'file_name' => 'Kiss_jolly_SS.png',
                'file_path' => '/cigarettes_images/Kiss_Richmond_2px/'
            ],
        ],
        'Sovereign' => [
            [
                'name' => 'Sovereign Blue',
                'type' => Cigarette::TYPE_KING_SIZE,
                'file_name' => 'Sovereign_KS.png',
                'file_path' => '/cigarettes_images/Sovereign_2px/'
            ],
        ],
    );

    /**
     * @return array
     */
    public function getCigarettes(): array
    {
        return $this->cigarettes;
    }

}
