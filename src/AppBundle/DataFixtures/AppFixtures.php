<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\DataFixtures;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Cigarette;
use AppBundle\Entity\User;
use AppBundle\Entity\Admin;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Vich\UploaderBundle\Handler\DownloadHandler;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder, DownloadHandler $downloadHandler)
    {
        $this->encoder = $encoder;
    }

    public function load(ObjectManager $manager)
    {
        $data = new Data();

        $admin = new Admin();
        $admin->setLogin('admin');
        $admin->setIsActive(true);
        $password = $this->encoder->encodePassword($admin, '123qwe');
        $admin->setPassword($password);
        $admin->setRole('ROLE_SUPER_ADMIN');
        $manager->persist($admin);

        $user = new User();
        $user->setLogin('user');
        $user->setFirstName('Василий');
        $user->setLastName('Пупкин');
        $user->setIsActive(true);
        $password = $this->encoder->encodePassword($user, '123qwe');
        $user->setPassword($password);
        $manager->persist($user);

        $manager->flush();

        foreach($data->getCigarettes() as $brandLabel => $cigarettes){
            $brand = new Brand();
            $brand->setName($brandLabel);
            $brand->setPriority(0);
            $manager->persist($brand);
            foreach($cigarettes as $cigaretteItem){
                $cigarette = new Cigarette();
                $cigarette->setName($cigaretteItem['name']);
                $cigarette->setType($cigaretteItem['type']);
                $cigarette->setBrand($brand);
                $image = $this->getImageFile($cigaretteItem['file_name'], $cigaretteItem['file_path']);
                $cigarette->setImageFile($image);
                $manager->persist($cigarette);
            }
        }

        $manager->flush();
    }

    private function getImageFile($fileName, $filePath, $mimeType = 'image/png'){
        $src = __DIR__.$filePath.$fileName;

        $fs = new Filesystem();
        $targetPath = sys_get_temp_dir().'/'.$src;
        $fs->copy($src, $targetPath, true);

        $file = new UploadedFile(
            $targetPath,
            $fileName,
            $mimeType,
            filesize($src),
            null,
            true //  Set test mode true !!! " Local files are used in test mode hence the code should not enforce HTTP uploads."
        );

        return $file;
    }
}
