<?php
namespace AppBundle\EventListener;

use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Liip\ImagineBundle\Imagine\Cache\CacheManager;
use Symfony\Component\HttpFoundation\RequestStack;
use Vich\UploaderBundle\Templating\Helper\UploaderHelper;
use AppBundle\Entity\ImagePathInterface;

/**
 * Class ImageListener
 * This class implements absolute paths of images for API controllers
 *
 * @package AppBundle\EventListener
 */
class ImageListener
{
    const API_CONTROLLER = 'AppBundle\Controller\Api';
    protected $requestStack;
    protected $uploaderHelper;
    protected $imagineCacheManager;

    public function __construct(RequestStack $requestStack, UploaderHelper $uploaderHelper, CacheManager $imagineCacheManager){
        $this->requestStack = $requestStack;
        $this->uploaderHelper = $uploaderHelper;
        $this->imagineCacheManager = $imagineCacheManager;
    }

    public function postLoad(LifecycleEventArgs $args){
        $entity = $args->getObject();
        $controller = $this->requestStack->getCurrentRequest()->attributes->get('_controller');
        if(strpos($controller, self::API_CONTROLLER) !== false){
            /** @var $entity ImagePathInterface */
            if($entity instanceof ImagePathInterface){
                if($entity->getImage()){
                    $resolvedPath = $this->imagineCacheManager->getBrowserPath(
                        $entity->getImage(), 'thumbnail'
                    );
                    $imageUrl = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost()
                        .$this->uploaderHelper->asset($entity, 'imageFile');

                    $entity->setImageUrl($imageUrl);
                    $entity->setImageThumbnailUrl($resolvedPath);
                }
            }
        }
    }
}