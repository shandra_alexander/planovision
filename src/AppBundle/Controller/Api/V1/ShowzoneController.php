<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Showzone;
use AppBundle\Service\ShowzoneService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

class ShowzoneController extends AbstractController
{
    public function __construct(ShowzoneService $service){
        parent::__construct($service);
    }

    /**
     * Get showzone types. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array of available showzone types.",
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No types were found"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     *
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/showzones/types")
     * @return View
     */
    public function getShowzoneTypes(): View
    {
        $data = $this->service->getShowzoneTypes();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * Get showzones. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of showzones.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Showzone::class))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="No Vendors have been found!"
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filters must be provided as an array of json objects.
           Acceptable Operator Values:
            eq | gt | lt | gte | lte | neq | in | notIn | like"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="string",
     *     description="asc or desc order"
     * )
     * @SWG\Parameter(
     *     name="sortField",
     *     in="query",
     *     type="string",
     *     description="Sort by field. Default sort by 'id' field"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Limit of returning objects. Default is 20"
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset of results objects, use for pagination. Default is 0"
     * )
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/showzones")
     *
     * @return View
     */
    public function getShowzonesAction(Request $request): View
    {
        return parent::getByAction($request);
    }

    /**
     * Get showzone by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=Showzone::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/showzones/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getShowzoneAction(int $id): View
    {
        return parent::getAction($id);
    }

    /**
     * Create new showzone. Authentication: User.
     *
     * @SWG\Post(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Cool adv"),
     *              @SWG\Property(property="type", type="integer", example=1),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the Showzone.",
     *     @Model(type=Showzone::class)
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/showzones")
     * @return View
     */
    public function createShowzoneAction(Request $request): View
    {
        return parent::createAction($request);
    }

    /**
     * Update showzone. Authentication: User.
     *
     * @SWG\Put(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload. You don't have to set all model parameters.
                             If you want to modify only one parameter, set in the JSON only one key and value",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Cool adv"),
     *              @SWG\Property(property="type", type="integer", example=1),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Empty response. It means that vendor has been updated.",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/showzones/{id}")
     * @return View
     */
    public function updateShowzoneAction(int $id, Request $request): View
    {
        return parent::updateAction($id, $request);
    }
}