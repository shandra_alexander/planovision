<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Brand;
use AppBundle\Service\ApiError;
use AppBundle\Service\BrandService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Nelmio\ApiDocBundle\Annotation\Security as NelmioSecurity;

class BrandController extends AbstractController
{
    public function __construct(BrandService $service){
        parent::__construct($service);
    }

    /**
     * Get brands. Authentication: User.
     *
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of brands.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Brand::class))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="No Resources Have been Found!"
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filters must be provided as an array of json objects.
           Acceptable Operator Values:
            eq | gt | lt | gte | lte | neq | in | notIn | like"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="string",
     *     description="asc or desc order"
     * )
     * @SWG\Parameter(
     *     name="sortField",
     *     in="query",
     *     type="string",
     *     description="Sort by field. Default sort by 'id' field"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Limit of returning objects. Default is 20"
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset of results objects, use for pagination. Default is 0"
     * )
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/brands")
     *
     * @return View
     */
    public function getBrandsAction(Request $request): View
    {
        return parent::getByAction($request);
    }

    /**
     * Get brand by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=Brand::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/brands/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getBrandAction(int $id): View
    {
        return parent::getAction($id);
    }

    /**
     * Craete new brand. Authentication: User.
     *
     * @SWG\Post(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Winston"),
     *              @SWG\Property(property="priority", type="integer", example=0),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the brand.",
     *     @Model(type=Brand::class)
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/brands")
     * @return View
     */
    public function createBrandAction(Request $request): View
    {
        return parent::createAction($request);
    }

    /**
     * Update brand. Authentication: User.
     *
     * @SWG\Put(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload. You don't have to set all model parameters.
                             If you want to modify only one parameter, set on JSON only one parameter and value",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Winston"),
     *              @SWG\Property(property="priority", type="integer", example=0),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Empty response. It means that brand has been updated. With your data",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/brands/{id}")
     * @return View
     */
    public function updateBrandAction(int $id, Request $request): View
    {
        return parent::updateAction($id, $request);
    }
}