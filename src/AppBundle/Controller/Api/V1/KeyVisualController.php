<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\KeyVisual;
use AppBundle\Service\KeyVisualService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

class KeyVisualController extends AbstractController
{
    public function __construct(KeyVisualService $service){
        parent::__construct($service);
    }

    /**
     * Get key visuals. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of key visuals.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=KeyVisual::class))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="No Vendors have been found!"
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filters must be provided as an array of json objects.
           Acceptable Operator Values:
            eq | gt | lt | gte | lte | neq | in | notIn | like"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="string",
     *     description="asc or desc order"
     * )
     * @SWG\Parameter(
     *     name="sortField",
     *     in="query",
     *     type="string",
     *     description="Sort by field. Default sort by 'id' field"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Limit of returning objects. Default is 20"
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset of results objects, use for pagination. Default is 0"
     * )
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/keyvisuals")
     *
     * @return View
     */
    public function getKeyVisualsAction(Request $request): View
    {
        return parent::getByAction($request);
    }

    /**
     * Get vendor by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=KeyVisual::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/keyvisuals/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getKeyVisualAction(int $id): View
    {
        return parent::getAction($id);
    }

    /**
     * Create new key visual. Authentication: User.
     *
     * @SWG\Post(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="title", type="string", example="Key visual"),
     *              @SWG\Property(property="widthRatio", type="integer", example=3),
     *              @SWG\Property(property="heightRatio", type="integer", example=4),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the key visual.",
     *     @Model(type=KeyVisual::class)
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/keyvisuals")
     * @return View
     */
    public function createKeyVisualAction(Request $request): View
    {
        return parent::createAction($request);
    }

    /**
     * Update key visual. Authentication: User.
     *
     * @SWG\Put(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload. You don't have to set all model parameters.
                             If you want to modify only one parameter, set on JSON only one parameter and value",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="title", type="string", example="Key visual"),
     *              @SWG\Property(property="widthRatio", type="integer", example=3),
     *              @SWG\Property(property="heightRatio", type="integer", example=4),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Empty response. It means that vendor has been updated.",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/keyvisuals/{id}")
     * @return View
     */
    public function updateKeyVisualAction(int $id, Request $request): View
    {
        return parent::updateAction($id, $request);
    }
}