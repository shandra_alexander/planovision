<?php

namespace AppBundle\Controller\Api\V1;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Entity\User;

class UserController extends FOSRestController
{
    /**
     * Get Self. Authentication: User.
     * @SWG\Response(
     *     response=200,
     *     description="Returns the self Object of User",
     *     @Model(type=User::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No Users Have been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/users/me")
     */
    public function getSelfAction()
    {
        $data = $this->getUser();
        $statusCode = $data ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;

        return new View($data, $statusCode);
    }

    /**
     * User Auth
     *
     * @SWG\Parameter(
     *     name="body",
     *     in="body",
     *     type="json",
     *     @SWG\Schema(type="object",
     *         @SWG\Property(property="username", type="string"),
     *         @SWG\Property(property="password", type="string")
     *     )
     * )
     * @SWG\Response(
     *     response=200,
     *     description="Returns user token",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request."
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Rest\Post("/login_check")
     */
    public function checkAction(){}
}