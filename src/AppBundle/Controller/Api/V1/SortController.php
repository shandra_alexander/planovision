<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Sort;
use AppBundle\Service\SortService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

class SortController extends AbstractController
{
    public function __construct(SortService $service){
        parent::__construct($service);
    }

    /**
     * Get sorts. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of all available sort items.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Sort::class))
     *     )
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No sort items were found"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/sorts")
     *
     * @return View
     */
    public function getAllSortsAction(): View
    {
        return parent::getAllAction();
    }

    /**
     * Get sort by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=Sort::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/sorts/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getSortAction(int $id): View
    {
        return parent::getAction($id);
    }
}