<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Cigarette;
use AppBundle\Service\CigaretteService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

class CigaretteController extends AbstractController
{
    public function __construct(CigaretteService $service){
        parent::__construct($service);
    }

    /**
     * Get cigarette types. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array of available cigarette types.",
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No types were found"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     *
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/cigarettes/types")
     * @return View
     */
    public function getCigaretteTypes(): View
    {
        $data = $this->service->getCigaretteTypes();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * Get cigarettes. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of cigarettes.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Cigarette::class))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="No Vendors have been found!"
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filters must be provided as an array of json objects.
           Acceptable Operator Values:
            eq | gt | lt | gte | lte | neq | in | notIn | like"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="string",
     *     description="asc or desc order"
     * )
     * @SWG\Parameter(
     *     name="sortField",
     *     in="query",
     *     type="string",
     *     description="Sort by field. Default sort by 'id' field"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Limit of returning objects. Default is 20"
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset of results objects, use for pagination. Default is 0"
     * )
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/cigarettes")
     *
     * @return View
     */
    public function getCigarettesAction(Request $request): View
    {
        return parent::getByAction($request);
    }

    /**
     * Get vendor by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=Cigarette::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/cigarettes/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getCigaretteAction(int $id): View
    {
        return parent::getAction($id);
    }

    /**
     * Create new cigarette. Authentication: User.
     *
     * @SWG\Post(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Small box"),
     *              @SWG\Property(property="type", type="integer", example=1),
     *              @SWG\Property(property="price", type="integer", example=50),
     *              @SWG\Property(property="brand", type="integer", example=1),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the cigarette.",
     *     @Model(type=Cigarette::class)
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/cigarettes")
     * @return View
     */
    public function createCigaretteAction(Request $request): View
    {
        return parent::createAction($request);
    }

    /**
     * Update cigarette. Authentication: User.
     *
     * @SWG\Put(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload. You don't have to set all model parameters.
                             If you want to modify only one parameter, set on JSON only one parameter and value",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Small box"),
     *              @SWG\Property(property="type", type="integer", example=1),
     *              @SWG\Property(property="price", type="integer", example=50),
     *              @SWG\Property(property="brand", type="integer", example=1),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Empty response. It means that vendor has been updated.",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/cigarettes/{id}")
     * @return View
     */
    public function updateCigaretteAction(int $id, Request $request): View
    {
        return parent::updateAction($id, $request);
    }
}