<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Cigarette;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\Showzone;
use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

class EnumController extends FOSRestController
{
    /**
     * Get Enums
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the enums"
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No Enums Have been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/enums")
     */
    public function getEnumsAction()
    {
        $data = [
            'CigaretteTypes' => Cigarette::$TYPES,
            'PlanogramTypes' => Planogram::$TYPES,
            'PlanogramShowzoneTypes' => Planogram::$SHOWZONE_TYPES,
            'ShowzoneTypes' => Showzone::$TYPES,
        ];

        return new View($data, Response::HTTP_OK);
    }
}