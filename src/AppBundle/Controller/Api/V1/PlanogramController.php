<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\PlanogramCigarette;
use AppBundle\Entity\Planogram;
use AppBundle\Service\PlanogramService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\Response;

class PlanogramController extends AbstractController
{
    public function __construct(PlanogramService $service){
        parent::__construct($service);
    }

    /**
     * Get planogram types. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array of available planogram types.",
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No types were found"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     *
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/planograms/types")
     * @return View
     */
    public function getPlanogramTypes(): View
    {
        $data = $this->service->getPlanogramTypes();
        return new View($data, Response::HTTP_OK);
    }

    /**
     * Get planogram showzone types. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the array of available planogram showzone types.",
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No types were found"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     *
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/planograms/showzonetypes")
     * @return View
     */
    public function getPlanogramShowzoneTypes(): View
    {
        $data = $this->service->getPlanogramShowzoneTypes();
        return new View($data, Response::HTTP_OK);
    }


    /**
     * Get planograms. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of planograms.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Planogram::class))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="No Vendors have been found!"
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filters must be provided as an array of json objects.
           Acceptable Operator Values:
            eq | gt | lt | gte | lte | neq | in | notIn | like"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="string",
     *     description="asc or desc order"
     * )
     * @SWG\Parameter(
     *     name="sortField",
     *     in="query",
     *     type="string",
     *     description="Sort by field. Default sort by 'id' field"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Limit of returning objects. Default is 20"
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset of results objects, use for pagination. Default is 0"
     * )
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/planograms")
     *
     * @return View
     */
    public function getPlanogramsAction(Request $request): View
    {
        return parent::getByAction($request);
    }

    /**
     * Get planogram by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=Planogram::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/planograms/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getPlanogramAction(int $id): View
    {
        return parent::getAction($id);
    }

    /**
     * Create new planogram. Authentication: User.
     *
     * @SWG\Post(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="type", type="integer", example=1),
     *              @SWG\Property(property="showzoneType", type="integer", example=1),
     *              @SWG\Property(property="keyVisual", type="integer", example=1),
     *              @SWG\Property(property="brand", type="integer", example=1),
     *              @SWG\Property(property="vendor", type="integer", example=1),
     *              @SWG\Property(property="stripe", type="integer", example=1),
     *              @SWG\Property(property="showzone", type="integer", example=1),
     *              @SWG\Property(property="sort", type="integer", example=1),
     *              @SWG\Property(
     *                  property="cigarettes",
     *                  type="array",
     *                  @SWG\Items(ref="#/definitions/PlanogramCigarette")
     *              ),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the planogram.",
     *     @Model(type=Planogram::class)
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/planograms")
     * @return View
     */
    public function createPlanogramAction(Request $request): View
    {
        return parent::createAction($request);
    }

    /**
     * Update planogram. Authentication: User.
     *
     * @SWG\Put(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload. You don't have to set all model parameters.
                             If you want to modify only one parameter, set on JSON only one parameter and value",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="type", type="integer", example=1),
     *              @SWG\Property(property="showzoneType", type="integer", example=1),
     *              @SWG\Property(property="keyVisual", type="integer", example=1),
     *              @SWG\Property(property="brand", type="integer", example=1),
     *              @SWG\Property(property="vendor", type="integer", example=1),
     *              @SWG\Property(property="stripe", type="integer", example=1),
     *              @SWG\Property(property="showzone", type="integer", example=1),
     *              @SWG\Property(property="sort", type="integer", example=1),
     *              @SWG\Property(
     *                  property="cigarettes",
     *                  type="array",
     *                  @SWG\Items(ref=@Model(type=PlanogramCigarette::class))
     *              ),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Empty response. It means that vendor has been updated.",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/planograms/{id}")
     * @return View
     */
    public function updatePlanogramAction(int $id, Request $request): View
    {
        return parent::updateAction($id, $request);
    }
}