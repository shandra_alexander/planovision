<?php

namespace AppBundle\Controller\Api\V1;

use AppBundle\Entity\Vendor;
use AppBundle\Service\VendorService;
use FOS\RestBundle\View\View;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

class VendorController extends AbstractController
{
    public function __construct(VendorService $service){
        parent::__construct($service);
    }

    /**
     * Get vendors. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the list of brands.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(ref=@Model(type=Vendor::class))
     *     )
     * )
     * @SWG\Response(
     *     response=404,
     *     description="No Vendors have been found!"
     * )
     * @SWG\Parameter(
     *     name="filters",
     *     in="query",
     *     type="string",
     *     description="Filters must be provided as an array of json objects.
           Acceptable Operator Values:
            eq | gt | lt | gte | lte | neq | in | notIn | like"
     * )
     * @SWG\Parameter(
     *     name="sort",
     *     in="query",
     *     type="string",
     *     description="asc or desc order"
     * )
     * @SWG\Parameter(
     *     name="sortField",
     *     in="query",
     *     type="string",
     *     description="Sort by field. Default sort by 'id' field"
     * )
     * @SWG\Parameter(
     *     name="limit",
     *     in="query",
     *     type="integer",
     *     description="Limit of returning objects. Default is 20"
     * )
     * @SWG\Parameter(
     *     name="offset",
     *     in="query",
     *     type="integer",
     *     description="Offset of results objects, use for pagination. Default is 0"
     * )
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/vendors")
     *
     * @return View
     */
    public function getVendorsAction(Request $request): View
    {
        return parent::getByAction($request);
    }

    /**
     * Get vendor by id. Authentication: User.
     *
     * @SWG\Response(
     *     response=200,
     *     description="Returns the resource with a given ID.",
     *     @Model(type=Vendor::class)
     * ),
     * @SWG\Response(
     *     response=404,
     *     description="No resource with such ID has been Found!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     * @Rest\Get("/vendors/{id}")
     *
     * @param int $id
     * @return View
     */
    public function getVendorAction(int $id): View
    {
        return parent::getAction($id);
    }

    /**
     * Craete new vendor. Authentication: User.
     *
     * @SWG\Post(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Small box"),
     *              @SWG\Property(property="width", type="integer", example=1000),
     *              @SWG\Property(property="height", type="integer", example=500),
     *              @SWG\Property(property="cols", type="integer", example=10),
     *              @SWG\Property(property="rows", type="integer", example=5),
     *              @SWG\Property(property="stripeHeight", type="integer", example=10),
     *              @SWG\Property(property="rowsHeight", type="integer", example=10),
     *              @SWG\Property(property="visualPosLeft", type="integer", example=10),
     *              @SWG\Property(property="visualPosTop", type="integer", example=10),
     *              @SWG\Property(property="visualWidth", type="integer", example=100),
     *              @SWG\Property(property="visualHeight", type="integer", example=100),
     *              @SWG\Property(property="innerPosLeft", type="integer", example=10),
     *              @SWG\Property(property="innerPosTop", type="integer", example=10),
     *              @SWG\Property(property="innerWidth", type="integer", example=400),
     *              @SWG\Property(property="innerHeight", type="integer", example=600),
     *              @SWG\Property(property="headerHeight", type="integer", example=300),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=201,
     *     description="Returns the vendor.",
     *     @Model(type=Vendor::class)
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Post("/vendors")
     * @return View
     */
    public function createVendorAction(Request $request): View
    {
        return parent::createAction($request);
    }

    /**
     * Update vendor. Authentication: User.
     *
     * @SWG\Put(
     *      @SWG\Parameter(
     *          name="body",
     *          in="body",
     *          description="JSON Payload. You don't have to set all model parameters.
                             If you want to modify only one parameter, set on JSON only one parameter and value",
     *          required=true,
     *          type="json",
     *          format="application/json",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="name", type="string", example="Small box"),
     *              @SWG\Property(property="width", type="integer", example=1000),
     *              @SWG\Property(property="height", type="integer", example=500),
     *              @SWG\Property(property="cols", type="integer", example=10),
     *              @SWG\Property(property="rows", type="integer", example=5),
     *              @SWG\Property(property="stripeHeight", type="integer", example=10),
     *              @SWG\Property(property="rowsHeight", type="integer", example=10),
     *              @SWG\Property(property="visualPosLeft", type="integer", example=10),
     *              @SWG\Property(property="visualPosTop", type="integer", example=10),
     *              @SWG\Property(property="visualWidth", type="integer", example=100),
     *              @SWG\Property(property="visualHeight", type="integer", example=100),
     *              @SWG\Property(property="innerPosLeft", type="integer", example=10),
     *              @SWG\Property(property="innerPosTop", type="integer", example=10),
     *              @SWG\Property(property="innerWidth", type="integer", example=400),
     *              @SWG\Property(property="innerHeight", type="integer", example=600),
     *              @SWG\Property(property="headerHeight", type="integer", example=300),
     *              @SWG\Property(property="imageFile", type="string", example="data:image/gif;base64,R0lGODlhAQABAIAAAP///wAAACH5BAEAAAAALAAAAAABAAEAAAICRAEAOw=="),
     *          )
     *      ),
     * )
     *
     * @SWG\Response(
     *     response=204,
     *     description="Empty response. It means that vendor has been updated.",
     * ),
     * @SWG\Response(
     *     response=400,
     *     description="Bad Request. Check if your Request is properly formed!"
     * ),
     * @SWG\Response(
     *     response=401,
     *     description="Unauthorized. Please provide JWT Token."
     * ),
     * @Security("is_granted('ROLE_USER')")
     *
     * @Rest\Put("/vendors/{id}")
     * @return View
     */
    public function updateVendorAction(int $id, Request $request): View
    {
        return parent::updateAction($id, $request);
    }
}