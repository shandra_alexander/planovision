<?php

namespace AppBundle\Controller\Api\V1;

use FOS\RestBundle\Controller\FOSRestController;
use FOS\RestBundle\View\View;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use AppBundle\Service\AbstractService;

abstract class AbstractController extends FOSRestController
{
    /**
     * @var AbstractService
     */
    protected $service;

    /**
	 * AbstractController constructor.
     *
	 * @param AbstractService $service
	 */
	public function __construct(AbstractService $service) {
		$this->service = $service;
	}

    /**
     * @return View
     */
    public function getAllAction(): View
    {
        return new View($this->service->findAll(), Response::HTTP_OK);
    }

    /**
     * @param $id
     * @return View
     */
    public function getAction($id): View
    {
        $data = $this->service->find($id);
	    $statusCode = $data ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;

        return new View($data, $statusCode);
    }

    /**
     * @param Request $request
     * @return View
     */
    public function createAction(Request $request): View
    {
        $data = $this->service->create($request);
        $statusCode = Response::HTTP_CREATED;
        if(!$data){
            $data = $this->service->getForm()->getErrors();
            $statusCode = Response::HTTP_BAD_REQUEST;
        }
        return new View($data, $statusCode);
    }
	
	/**
	 *
	 * @param int id
	 * @param Request $request
	 * @return View
	 */
	public function updateAction(int $id, Request $request): View
	{
        $data = $this->service->update($id, $request);
        $statusCode = Response::HTTP_NO_CONTENT;
        if(!$data){
            $data = $this->service->getForm() ? $this->service->getForm()->getErrors() : null;
            $statusCode = Response::HTTP_BAD_REQUEST;
        }
        return new View($data, $statusCode);
	}

    /**
     * @return View
     */
    public function getByAction(Request $request): View
    {
        $filters = $request->query->get('filters') ? json_decode($request->query->get('filters'), true) : array();

        if(is_null($filters)){
            // It means that filters is invalid json as json could not be decoded
            $data = null;
            $statusCode = Response::HTTP_BAD_REQUEST;
        }else{
            $sortParameter = $request->query->get('sort');
            $sortFieldParameter = $request->query->get('sortField');
            $limitParameter = $request->query->get('limit');
            $offsetParameter = $request->query->get('offset');

            $data = $this->service->findBy($filters, $sortParameter, $sortFieldParameter, $limitParameter, $offsetParameter);
            $statusCode = $data ? Response::HTTP_OK : Response::HTTP_NOT_FOUND;
        }

        return new View($data, $statusCode);
    }
}
