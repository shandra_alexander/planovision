<?php
/**
 * Created by PhpStorm.
 * User: sd
 * Date: 5/15/19
 * Time: 6:14 AM
 */

namespace AppBundle\Entity;


interface ImagePathInterface
{
    public function getImage();
    public function setImageUrl(string $url);
    public function getImageUrl();
    public function setImageThumbnailUrl(string $url);
    public function getImageThumbnailUrl();
}