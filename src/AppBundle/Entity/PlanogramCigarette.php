<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use Swagger\Annotations as SWG;

/**
 * PlanogramCigarette
 *
 * @ORM\Table(name="planograms_cigarettes")
 * @ORM\Entity
 */
class PlanogramCigarette implements EntityInterface
{

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @Exclude()
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Planogram", inversedBy="cigarettes", cascade={"persist"})
     * @ORM\JoinColumn(name="planogram_id", referencedColumnName="id", nullable=false)
     */
    private $planogram;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Cigarette", inversedBy="planograms", cascade={"persist"})
     * @ORM\JoinColumn(name="cigarette_id", referencedColumnName="id", nullable=false)
     *
     * @SWG\Property(
     *     type="integer",
     *     description="The identifier of the cigarette."
     * )
     */
    private $cigarette;

    /**
     * @var int
     *
     * @ORM\Column(name="col", type="integer")
     */
    private $col;

    /**
     * @var int
     *
     * @ORM\Column(name="row", type="integer")
     */
    private $row;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set col
     *
     * @param integer $col
     *
     * @return PlanogramCigarette
     */
    public function setCol($col)
    {
        $this->col = $col;

        return $this;
    }

    /**
     * Get col
     *
     * @return integer
     */
    public function getCol()
    {
        return $this->col;
    }

    /**
     * Set row
     *
     * @param integer $row
     *
     * @return PlanogramCigarette
     */
    public function setRow($row)
    {
        $this->row = $row;

        return $this;
    }

    /**
     * Get row
     *
     * @return integer
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     *
     * @return PlanogramCigarette
     */
    public function setPlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planogram = $planogram;

        return $this;
    }

    /**
     * Get planogram
     *
     * @return \AppBundle\Entity\Planogram
     */
    public function getPlanogram()
    {
        return $this->planogram;
    }

    /**
     * Set cigarette
     *
     * @param \AppBundle\Entity\Cigarette $cigarette
     *
     * @return PlanogramCigarette
     */
    public function setCigarette(\AppBundle\Entity\Cigarette $cigarette)
    {
        $this->cigarette = $cigarette;

        return $this;
    }

    /**
     * Get cigarette
     *
     * @return \AppBundle\Entity\Cigarette
     */
    public function getCigarette()
    {
        return $this->cigarette;
    }

    public function __toString()
    {
        return (string)$this->getId();
    }
}
