<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Cigarette
 *
 * @ORM\Table(name="cigarette")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CigaretteRepository")
 * @Vich\Uploadable
 */
class Cigarette implements EntityInterface, ImagePathInterface
{
    CONST TYPE_COMPACT = 1;
    CONST TYPE_KING_SIZE = 2;
    CONST TYPE_SUPER_SLIMS = 3;
    CONST TYPE_KING_SIZE_SUPER_SLIMS = 4;
    CONST TYPE_KING_SIZE_SUPER_SLIMS_EXTRA = 5;
    CONST TYPE_UNDEFINED = 6;

    /**
     * @Exclude()
     */
    public static $TYPES = [
        'Компакт' => self::TYPE_COMPACT,
        'Кинг сайз' => self::TYPE_KING_SIZE,
        'Супер слимс' => self::TYPE_SUPER_SLIMS,
        'Кинг сайз супер слимс' => self::TYPE_KING_SIZE_SUPER_SLIMS,
        'Экстра кинг сайз супер слимс' => self::TYPE_KING_SIZE_SUPER_SLIMS_EXTRA,
        'Другое' => self::TYPE_UNDEFINED,
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @SWG\Property(description="The unique identifier of the cigarette.")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=150)
     *
     * @SWG\Property(description="The name of the cigarette.")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", options={"default" = 1})
     *
     *
     */
    private $type = self::TYPE_COMPACT;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", options={"default" = 0})
     */
    private $price = 0;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Brand", inversedBy="cigarettes", cascade={"persist"})
     * @ORM\JoinColumn(name="brand_id", referencedColumnName="id", nullable=false)
     * @MaxDepth(1)
     */
    private $brand;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\PlanogramCigarette", mappedBy="cigarette")
     */
    private $planograms;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="cigarette_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedImageAt;

    private $imageUrl;
    private $imageThumbnailUrl;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Cigarette
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Cigarette
     */
    public function setType($type)
    {
        if (!in_array($type, array_values(self::$TYPES))) {
            throw new \InvalidArgumentException("Invalid type");
        }
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return integer
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set brand
     *
     * @param \AppBundle\Entity\Brand $brand
     *
     * @return Cigarette
     */
    public function setBrand(\AppBundle\Entity\Brand $brand)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \AppBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedImageAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set updatedImageAt
     *
     * @param \DateTime $updatedImageAt
     *
     * @return Cigarette
     */
    public function setUpdatedImageAt($updatedImageAt)
    {
        $this->updatedImageAt = $updatedImageAt;

        return $this;
    }

    /**
     * Get updatedImageAt
     *
     * @return \DateTime
     */
    public function getUpdatedImageAt()
    {
        return $this->updatedImageAt;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->planograms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add planogram
     *
     * @param \AppBundle\Entity\PlanogramCigarette $planogram
     *
     * @return Cigarette
     */
    public function addPlanogram(\AppBundle\Entity\PlanogramCigarette $planogram)
    {
        $planogram->setCigarette($this);
        $this->planograms[] = $planogram;

        return $this;
    }

    /**
     * Remove planogram
     *
     * @param \AppBundle\Entity\PlanogramCigarette $planogram
     */
    public function removePlanogram(\AppBundle\Entity\PlanogramCigarette $planogram)
    {
        $this->planograms->removeElement($planogram);
    }

    /**
     * Get planograms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanograms()
    {
        return $this->planograms;
    }

    public function getTypeLabel(){
        return array_search($this->getType(), self::$TYPES);
    }

    public function __toString()
    {
        return $this->getName()." (".$this->getTypeLabel().")";
    }

    /**
     * Set price
     *
     * @param integer $price
     *
     * @return Cigarette
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return integer
     */
    public function getPrice()
    {
        return $this->price;
    }

    public function setImageUrl(string $url)
    {
        $this->imageUrl = $url;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setImageThumbnailUrl(string $url)
    {
        $this->imageThumbnailUrl = $url;
    }

    public function getImageThumbnailUrl()
    {
        return $this->imageThumbnailUrl;
    }
}
