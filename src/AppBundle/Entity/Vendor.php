<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Vendor
 *
 * @ORM\Table(name="vendor")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VendorRepository")
 * @ORM\HasLifecycleCallbacks
 * @Vich\Uploadable
 */
class Vendor implements EntityInterface, ImagePathInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(name="width", type="integer")
     */
    private $width;

    /**
     * @var int
     *
     * @ORM\Column(name="height", type="integer")
     */
    private $height;

    /**
     * @var int
     *
     * @ORM\Column(name="cols", type="integer")
     */
    private $cols;

    /**
     * @var int
     *
     * @ORM\Column(name="rows", type="integer")
     */
    private $rows;

    /**
     * @var int
     *
     * @ORM\Column(name="stripe_height", type="integer", nullable=true)
     */
    private $stripeHeight;

    /**
     * @var int
     *
     * @ORM\Column(name="rows_height", type="integer")
     */
    private $rowsHeight;

    /**
     * @var int
     *
     * @ORM\Column(name="visual_pos_left", type="integer", nullable=true)
     */
    private $visualPosLeft;

    /**
     * @var int
     *
     * @ORM\Column(name="visual_pos_top", type="integer", nullable=true)
     */
    private $visualPosTop;

    /**
     * @var int
     *
     * @ORM\Column(name="visual_width", type="integer")
     */
    private $visualWidth;

    /**
     * @var int
     *
     * @ORM\Column(name="visual_height", type="integer")
     */
    private $visualHeight;

    /**
     * @var int
     *
     * @ORM\Column(name="inner_pos_left", type="integer")
     */
    private $innerPosLeft;

    /**
     * @var int
     *
     * @ORM\Column(name="inner_pos_top", type="integer")
     */
    private $innerPosTop;

    /**
     * @var int
     *
     * @ORM\Column(name="inner_width", type="integer")
     */
    private $innerWidth;

    /**
     * @var int
     *
     * @ORM\Column(name="inner_height", type="integer")
     */
    private $innerHeight;

    /**
     * @var int
     *
     * @ORM\Column(name="header_height", type="integer", nullable=true)
     */
    private $headerHeight;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Planogram", mappedBy="vendor")
     */
    private $planograms;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="vendor_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    private $imageUrl;
    private $imageThumbnailUrl;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Vendor
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return Vendor
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return int
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return Vendor
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return int
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set cols
     *
     * @param integer $cols
     *
     * @return Vendor
     */
    public function setCols($cols)
    {
        $this->cols = $cols;

        return $this;
    }

    /**
     * Get cols
     *
     * @return int
     */
    public function getCols()
    {
        return $this->cols;
    }

    /**
     * Set rows
     *
     * @param integer $rows
     *
     * @return Vendor
     */
    public function setRows($rows)
    {
        $this->rows = $rows;

        return $this;
    }

    /**
     * Get rows
     *
     * @return int
     */
    public function getRows()
    {
        return $this->rows;
    }

    /**
     * Set stripeHeight
     *
     * @param integer $stripeHeight
     *
     * @return Vendor
     */
    public function setStripeHeight($stripeHeight)
    {
        $this->stripeHeight = $stripeHeight;

        return $this;
    }

    /**
     * Get stripeHeight
     *
     * @return int
     */
    public function getStripeHeight()
    {
        return $this->stripeHeight;
    }

    /**
     * Set rowsHeight
     *
     * @param integer $rowsHeight
     *
     * @return Vendor
     */
    public function setRowsHeight($rowsHeight)
    {
        $this->rowsHeight = $rowsHeight;

        return $this;
    }

    /**
     * Get rowsHeight
     *
     * @return int
     */
    public function getRowsHeight()
    {
        return $this->rowsHeight;
    }

    /**
     * Set visualPosLeft
     *
     * @param integer $visualPosLeft
     *
     * @return Vendor
     */
    public function setVisualPosLeft($visualPosLeft)
    {
        $this->visualPosLeft = $visualPosLeft;

        return $this;
    }

    /**
     * Get visualPosLeft
     *
     * @return int
     */
    public function getVisualPosLeft()
    {
        return $this->visualPosLeft;
    }

    /**
     * Set visualPosTop
     *
     * @param integer $visualPosTop
     *
     * @return Vendor
     */
    public function setVisualPosTop($visualPosTop)
    {
        $this->visualPosTop = $visualPosTop;

        return $this;
    }

    /**
     * Get visualPosTop
     *
     * @return int
     */
    public function getVisualPosTop()
    {
        return $this->visualPosTop;
    }

    /**
     * Set innerPosLeft
     *
     * @param integer $innerPosLeft
     *
     * @return Vendor
     */
    public function setInnerPosLeft($innerPosLeft)
    {
        $this->innerPosLeft = $innerPosLeft;

        return $this;
    }

    /**
     * Get innerPosLeft
     *
     * @return int
     */
    public function getInnerPosLeft()
    {
        return $this->innerPosLeft;
    }

    /**
     * Set innerPosTop
     *
     * @param integer $innerPosTop
     *
     * @return Vendor
     */
    public function setInnerPosTop($innerPosTop)
    {
        $this->innerPosTop = $innerPosTop;

        return $this;
    }

    /**
     * Get innerPosTop
     *
     * @return int
     */
    public function getInnerPosTop()
    {
        return $this->innerPosTop;
    }

    /**
     * Set headerHeight
     *
     * @param integer $headerHeight
     *
     * @return Vendor
     */
    public function setHeaderHeight($headerHeight)
    {
        $this->headerHeight = $headerHeight;

        return $this;
    }

    /**
     * Get headerHeight
     *
     * @return int
     */
    public function getHeaderHeight()
    {
        return $this->headerHeight;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Vendor
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Vendor
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Add planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     *
     * @return Vendor
     */
    public function addPlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms[] = $planogram;

        return $this;
    }

    /**
     * Remove planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     */
    public function removePlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms->removeElement($planogram);
    }

    /**
     * Get planograms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanograms()
    {
        return $this->planograms;
    }

    public function __toString()
    {
        return $this->getName();
    }


    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set innerWidth
     *
     * @param integer $innerWidth
     *
     * @return Vendor
     */
    public function setInnerWidth($innerWidth)
    {
        $this->innerWidth = $innerWidth;

        return $this;
    }

    /**
     * Get innerWidth
     *
     * @return integer
     */
    public function getInnerWidth()
    {
        return $this->innerWidth;
    }

    /**
     * Set innerHeight
     *
     * @param integer $innerHeight
     *
     * @return Vendor
     */
    public function setInnerHeight($innerHeight)
    {
        $this->innerHeight = $innerHeight;

        return $this;
    }

    /**
     * Get innerHeight
     *
     * @return integer
     */
    public function getInnerHeight()
    {
        return $this->innerHeight;
    }

    /**
     * Set visualWidth
     *
     * @param integer $visualWidth
     *
     * @return Vendor
     */
    public function setVisualWidth($visualWidth)
    {
        $this->visualWidth = $visualWidth;

        return $this;
    }

    /**
     * Get visualWidth
     *
     * @return integer
     */
    public function getVisualWidth()
    {
        return $this->visualWidth;
    }

    /**
     * Set visualHeight
     *
     * @param integer $visualHeight
     *
     * @return Vendor
     */
    public function setVisualHeight($visualHeight)
    {
        $this->visualHeight = $visualHeight;

        return $this;
    }

    /**
     * Get visualHeight
     *
     * @return integer
     */
    public function getVisualHeight()
    {
        return $this->visualHeight;
    }

    public function setImageUrl(string $url)
    {
        $this->imageUrl = $url;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setImageThumbnailUrl(string $url)
    {
        $this->imageThumbnailUrl = $url;
    }

    public function getImageThumbnailUrl()
    {
        return $this->imageThumbnailUrl;
    }
}
