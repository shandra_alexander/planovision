<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Stripe
 *
 * @ORM\Table(name="stripe")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\StripeRepository")
 * @Vich\Uploadable
 */
class Stripe implements EntityInterface, ImagePathInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Planogram", mappedBy="stripe")
     */
    private $planograms;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Brand", inversedBy="stripes")
     */
    private $brand;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="stripe_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     * @var \DateTime
     */
    private $updatedImageAt;

    private $imageUrl;
    private $imageThumbnailUrl;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->planograms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     *
     * @return Stripe
     */
    public function addPlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms[] = $planogram;

        return $this;
    }

    /**
     * Remove planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     */
    public function removePlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms->removeElement($planogram);
    }

    /**
     * Get planograms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanograms()
    {
        return $this->planograms;
    }

    /**
     * Set brand
     *
     * @param \AppBundle\Entity\Brand $brand
     *
     * @return Stripe
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \AppBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Stripe
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function __toString()
    {
        return $this->getTitle();
    }


    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedImageAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set updatedImageAt
     *
     * @param \DateTime $updatedImageAt
     *
     * @return Stripe
     */
    public function setUpdatedImageAt($updatedImageAt)
    {
        $this->updatedImageAt = $updatedImageAt;

        return $this;
    }

    /**
     * Get updatedImageAt
     *
     * @return \DateTime
     */
    public function getUpdatedImageAt()
    {
        return $this->updatedImageAt;
    }

    public function setImageUrl(string $url)
    {
        $this->imageUrl = $url;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setImageThumbnailUrl(string $url)
    {
        $this->imageThumbnailUrl = $url;
    }

    public function getImageThumbnailUrl()
    {
        return $this->imageThumbnailUrl;
    }
}
