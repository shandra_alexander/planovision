<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Vich\UploaderBundle\Mapping\Annotation as Vich;

/**
 * KeyVisual
 *
 * @ORM\Table(name="key_visual")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\KeyVisualRepository")
 * @Vich\Uploadable
 */
class KeyVisual
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="key_visual_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedImageAt;

    /**
     * @var int
     *
     * @ORM\Column(name="width_ratio", type="integer")
     */
    private $widthRatio;

    /**
     * @var int
     *
     * @ORM\Column(name="height_ratio", type="integer")
     */
    private $heightRatio;

    /**
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Planogram",
     *      mappedBy="keyVisual",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     */
    private $planograms;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return KeyVisual
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedImageAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set updatedImageAt.
     *
     * @param \DateTime $updatedImageAt
     *
     * @return KeyVisual
     */
    public function setUpdatedImageAt($updatedImageAt)
    {
        $this->updatedImageAt = $updatedImageAt;

        return $this;
    }

    /**
     * Get updatedImageAt.
     *
     * @return \DateTime
     */
    public function getUpdatedImageAt()
    {
        return $this->updatedImageAt;
    }

    /**
     * Set widthRatio
     *
     * @param integer $widthRatio
     *
     * @return KeyVisual
     */
    public function setWidthRatio($widthRatio)
    {
        $this->widthRatio = $widthRatio;

        return $this;
    }

    /**
     * Get widthRatio
     *
     * @return integer
     */
    public function getWidthRatio()
    {
        return $this->widthRatio;
    }

    /**
     * Set heightRatio
     *
     * @param integer $heightRatio
     *
     * @return KeyVisual
     */
    public function setHeightRatio($heightRatio)
    {
        $this->heightRatio = $heightRatio;

        return $this;
    }

    /**
     * Get heightRatio
     *
     * @return integer
     */
    public function getHeightRatio()
    {
        return $this->heightRatio;
    }
}
