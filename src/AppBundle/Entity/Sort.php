<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;

/**
 * Sort
 *
 * @ORM\Table(name="planovision_sort")
 * @ORM\Entity
 */
class Sort implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=160)
     */
    private $name;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Planogram", mappedBy="sort")
     */
    private $planograms;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->planograms = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     *
     * @return Sort
     */
    public function addPlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms[] = $planogram;

        return $this;
    }

    /**
     * Remove planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     */
    public function removePlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms->removeElement($planogram);
    }

    /**
     * Get planograms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanograms()
    {
        return $this->planograms;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Sort
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    public function __toString()
    {
        return $this->getName();
    }
}
