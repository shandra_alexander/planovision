<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\AdvancedUserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
/**
 * @ORM\Table(name="base_user")
 * @ORM\Entity()
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="account_type", type="string")
 * @ORM\DiscriminatorMap({"admin" = "Admin",
 *                        "user" = "User"})
 * @Vich\Uploadable
 * @UniqueEntity(fields="login", message="Email already taken")
 */
abstract class BaseUser implements AdvancedUserInterface, \Serializable, EntityInterface
{
    /**
     * @SWG\Property(description="The unique ID of the user.")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @SWG\Property(description="The login of the user. Must be unique.")
     * @ORM\Column(type="string", length=100, unique=true)
     * @Assert\NotBlank()
     */
    private $login;

    /**
     * @SWG\Property(description="Plain Password. Not persisted into DB")
     * @Assert\Length(max=255)
     */
    private $plainPassword;

    /**
     * @Exclude() // not safe
     * @SWG\Property(description="Password of user. Stores BCRYPT encoded password.")
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(max=255)
     */
    private $password;

    /**
     * @Exclude() // not necessary
     * @SWG\Property(description="Shows if user account is active.")
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive = false;

    /**
     * @Exclude() // not necessary
     * @SWG\Property(description="Date when User profile was created.")
     * @ORM\Column(type="datetime")
     */
    private $dateCreated;

    /**
     * @SWG\Property(description="User Account's Role.")
     * @ORM\Column(type="string")
     */
    private $role;

    /**
     * @MaxDepth(1)
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Planogram",
     *      mappedBy="user",
     *      orphanRemoval=true,
     *      cascade={"persist", "remove"}
     * )
     */
    private $planograms;

    function __construct()
    {
        //default account role: ROLE_USER
        $this->setRole('ROLE_USER');
        //set Date Created as Now
        $this->setDateCreated(new \DateTime);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set password
     *
     * @param string $password
     *
     * @return BaseUser
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set isActive
     *
     * @param boolean $isActive
     *
     * @return BaseUser
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;

        return $this;
    }

    /**
     * Get isActive
     *
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * Set dateCreated
     *
     * @param \DateTime $dateCreated
     *
     * @return BaseUser
     */
    public function setDateCreated($dateCreated)
    {
        $this->dateCreated = $dateCreated;

        return $this;
    }

    /**
     * Get dateCreated
     *
     * @return \DateTime
     */
    public function getDateCreated()
    {
        return $this->dateCreated;
    }

    /**
     * Set role
     *
     * @param string $role
     *
     * @return BaseUser
     */
    public function setRole($role)
    {
        $this->role = $role;

        return $this;
    }

    /**
     * Get role
     *
     * @return string
     */
    public function getRole()
    {
        return $this->role;
    }
    
    //Serilizable Methods Implementation

    public function serialize(): string
    {
        return serialize(array(
            $this->id,
            $this->login,
            $this->password,
            $this->isActive,
        ));
    }

    public function unserialize($serialized): void
    {
        list (
            $this->id,
            $this->login,
            $this->password,
            $this->isActive,
            ) = unserialize($serialized);
    }

    //UserInterface Methods Implementation
    public function getSalt()
    {
        
    }

    public function getRoles()
    {
        return array($this->role);
    }

    public function eraseCredentials()
    {
        
    }

    //repeats itself with getEmail. However is
    // necessary for implementation of UserInterface
    public function getUsername()
    {
        return $this->login;
    }

    //AdvancedUserInterface Methods Implementation
    public function isAccountNonExpired()
    {
        return true;
    }

    public function isAccountNonLocked()
    {
        return true;
    }

    public function isCredentialsNonExpired()
    {
        return true;
    }

    public function isEnabled()
    {
        return $this->isActive;
    }
    
    public function getPlainPassword()
    {
        return $this->plainPassword;
    }

    public function setPlainPassword($password)
    {
        $this->plainPassword = $password;
    }

    /**
     * Set login
     *
     * @param string $login
     *
     * @return BaseUser
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Add planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     *
     * @return BaseUser
     */
    public function addPlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms[] = $planogram;

        return $this;
    }

    /**
     * Remove planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     */
    public function removePlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms->removeElement($planogram);
    }

    /**
     * Get planograms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanograms()
    {
        return $this->planograms;
    }
}
