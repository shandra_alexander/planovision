<?php
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM; 
use Symfony\Component\HttpFoundation\File\File; 
use Vich\UploaderBundle\Mapping\Annotation as Vich; 
use Symfony\Component\Validator\Constraints as Assert;
/**
 * @ORM\Entity
 * @ORM\Table(name="attachment")
 * @Vich\Uploadable
 */
class Attachment implements EntityInterface
{
    /** 
     * @var int 
     * @ORM\Column(name="id", type="integer") 
     * @ORM\Id 
     * @ORM\GeneratedValue(strategy="AUTO") 
     */ 
    private $id;
    
    /**
     * @ORM\Column(type="string", length=255)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="tree_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     * @var \DateTime
     */
    private $updatedImageAt;

    public $imageUrl;
    public $imageThumbnailUrl;
    
    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedImageAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set updatedImageAt.
     *
     * @param \DateTime $updatedImageAt
     *
     * @return Attachment
     */
    public function setUpdatedImageAt($updatedImageAt)
    {
        $this->updatedImageAt = $updatedImageAt;

        return $this;
    }

    /**
     * Get updatedImageAt.
     *
     * @return \DateTime
     */
    public function getUpdatedImageAt()
    {
        return $this->updatedImageAt;
    }

}
