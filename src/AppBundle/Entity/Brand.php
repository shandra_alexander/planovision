<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Symfony\Component\HttpFoundation\File\File;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Brand
 *
 * @ORM\Table(name="brand")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\BrandRepository")
 * @Vich\Uploadable
 */
class Brand implements EntityInterface, ImagePathInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @SWG\Property(description="The unique identifier of the brand.")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=60)
     * @Assert\NotBlank()
     *
     * @SWG\Property(description="The name of the brand")
     */
    private $name;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Cigarette", mappedBy="brand")
     */
    private $cigarettes;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Planogram", mappedBy="brand")
     */
    private $planograms;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Stripe", mappedBy="brand")
     */
    private $stripes;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\KeyVisual", mappedBy="brand")
     */
    private $keyVisuals;

    /**
     * @Exclude()
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\Showzone", mappedBy="brand")
     */
    private $showzones;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @var string
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="brand_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime", options={"default": "CURRENT_TIMESTAMP"}, nullable=true)
     * @var \DateTime
     */
    private $updatedImageAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="priority", type="smallint")
     * @Assert\NotBlank()
     *
     * @SWG\Property(description="The priority of the brand")
     */
    private $priority;

    private $imageUrl;
    private $imageThumbnailUrl;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->cigarettes = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Brand
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Add cigarette
     *
     * @param \AppBundle\Entity\Cigarette $cigarette
     *
     * @return Brand
     */
    public function addCigarette(\AppBundle\Entity\Cigarette $cigarette)
    {
        $this->cigarettes[] = $cigarette;

        return $this;
    }

    /**
     * Remove cigarette
     *
     * @param \AppBundle\Entity\Cigarette $cigarette
     */
    public function removeCigarette(\AppBundle\Entity\Cigarette $cigarette)
    {
        $this->cigarettes->removeElement($cigarette);
    }

    /**
     * Get cigarettes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCigarettes()
    {
        return $this->cigarettes;
    }

    public function setImageFile(File $image = null)
    {
        $this->imageFile = $image;

        // VERY IMPORTANT:
        // It is required that at least one field changes if you are using Doctrine,
        // otherwise the event listeners won't be called and the file is lost
        if ($image) {
            // if 'updatedAt' is not defined in your entity, use another property
            $this->updatedImageAt = new \DateTime('now');
        }
    }

    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set updatedImageAt
     *
     * @param \DateTime $updatedImageAt
     *
     * @return Brand
     */
    public function setUpdatedImageAt($updatedImageAt)
    {
        $this->updatedImageAt = $updatedImageAt;

        return $this;
    }

    /**
     * Get updatedImageAt
     *
     * @return \DateTime
     */
    public function getUpdatedImageAt()
    {
        return $this->updatedImageAt;
    }


    public function __toString()
    {
        return $this->getName();
    }

    /**
     * Add planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     *
     * @return Brand
     */
    public function addPlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms[] = $planogram;

        return $this;
    }

    /**
     * Remove planogram
     *
     * @param \AppBundle\Entity\Planogram $planogram
     */
    public function removePlanogram(\AppBundle\Entity\Planogram $planogram)
    {
        $this->planograms->removeElement($planogram);
    }

    /**
     * Get planograms
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getPlanograms()
    {
        return $this->planograms;
    }

    /**
     * Add stripe
     *
     * @param \AppBundle\Entity\Stripe $stripe
     *
     * @return Brand
     */
    public function addStripe(\AppBundle\Entity\Stripe $stripe)
    {
        $this->stripes[] = $stripe;

        return $this;
    }

    /**
     * Remove stripe
     *
     * @param \AppBundle\Entity\Stripe $stripe
     */
    public function removeStripe(\AppBundle\Entity\Stripe $stripe)
    {
        $this->stripes->removeElement($stripe);
    }

    /**
     * Get stripes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getStripes()
    {
        return $this->stripes;
    }

    /**
     * Add keyVisual
     *
     * @param \AppBundle\Entity\KeyVisual $keyVisual
     *
     * @return Brand
     */
    public function addKeyVisual(\AppBundle\Entity\KeyVisual $keyVisual)
    {
        $this->keyVisuals[] = $keyVisual;

        return $this;
    }

    /**
     * Remove keyVisual
     *
     * @param \AppBundle\Entity\KeyVisual $keyVisual
     */
    public function removeKeyVisual(\AppBundle\Entity\KeyVisual $keyVisual)
    {
        $this->keyVisuals->removeElement($keyVisual);
    }

    /**
     * Get keyVisuals
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getKeyVisuals()
    {
        return $this->keyVisuals;
    }

    /**
     * Add showzone
     *
     * @param \AppBundle\Entity\Showzone $showzone
     *
     * @return Brand
     */
    public function addShowzone(\AppBundle\Entity\Showzone $showzone)
    {
        $this->showzones[] = $showzone;

        return $this;
    }

    /**
     * Remove showzone
     *
     * @param \AppBundle\Entity\Showzone $showzone
     */
    public function removeShowzone(\AppBundle\Entity\Showzone $showzone)
    {
        $this->showzones->removeElement($showzone);
    }

    /**
     * Get showzones
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getShowzones()
    {
        return $this->showzones;
    }

    public function setImageUrl(string $url)
    {
        $this->imageUrl = $url;
    }

    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    public function setImageThumbnailUrl(string $url)
    {
        $this->imageThumbnailUrl = $url;
    }

    public function getImageThumbnailUrl()
    {
        return $this->imageThumbnailUrl;
    }

    /**
     * Set priority
     *
     * @param integer $priority
     *
     * @return Brand
     */
    public function setPriority($priority)
    {
        $this->priority = $priority;

        return $this;
    }

    /**
     * Get priority
     *
     * @return integer
     */
    public function getPriority()
    {
        return $this->priority;
    }
}
