<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation\Exclude;
use Swagger\Annotations as SWG;
use AppBundle\Entity\BaseUser;
/**
 * 
 * @ORM\Entity
 * @ORM\Table(name="admin")
 */
class Admin extends BaseUser
{
    /**
     * @Exclude() // not safe or necessary
     * @SWG\Property(description="Admin's Access Array.")
     * @ORM\Column(type="array")
     */
    private $access;

    /**
     * Set access
     *
     * @param array $access
     *
     * @return Admin
     */
    public function setAccess($access)
    {
        $this->access = $access;

        return $this;
    }

    /**
     * Get access
     *
     * @return array
     */
    public function getAccess()
    {
        return $this->access;
    }

    public function __toString()
    {
        return parent::getUsername();
    }
}
