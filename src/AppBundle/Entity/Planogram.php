<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\Exclude;
use JMS\Serializer\Annotation\MaxDepth;

/**
 * Planogram
 *
 * @ORM\Table(name="planogram")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PlanogramRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Planogram implements EntityInterface
{
    CONST TYPE_STANDARD = 1;
    CONST TYPE_MIRROR = 2;
    CONST SHOWZONE_TYPE_LEFT = 1;
    CONST SHOWZONE_TYPE_RIGHT = 2;

    /**
     * @Exclude()
     */
    public static $TYPES = [
        'Обычная' => self::TYPE_STANDARD,
        'Зеркальная' => self::TYPE_MIRROR
    ];

    /**
     * @Exclude()
     */
    public static $SHOWZONE_TYPES = [
        'Лево' => self::SHOWZONE_TYPE_LEFT,
        'Право' => self::SHOWZONE_TYPE_RIGHT
    ];

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="type", type="integer", options={"default" = 1})
     */
    private $type = self::TYPE_STANDARD;

    /**
     * @var int
     *
     * @ORM\Column(name="showzone_type", type="integer", nullable=true)
     */
    private $showzoneType;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $createdAt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @MaxDepth(1)
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\BaseUser", inversedBy="planograms")
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\KeyVisual", inversedBy="planograms")
     */
    private $keyVisual;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Brand", inversedBy="planograms")
     */
    private $brand;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Vendor", inversedBy="planograms")
     */
    private $vendor;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Stripe", inversedBy="planograms")
     * @ORM\JoinColumn(name="stripe_id", referencedColumnName="id", nullable=true)
     */
    private $stripe;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Showzone", inversedBy="planograms")
     * @ORM\JoinColumn(name="showzone_id", referencedColumnName="id", nullable=true)
     */
    private $showzone;

    /**
     * @ORM\ManyToOne(targetEntity="\AppBundle\Entity\Sort", inversedBy="planograms")
     * @ORM\JoinColumn(name="sort_id", referencedColumnName="id", nullable=true)
     */
    private $sort;

    /**
     * @MaxDepth(2)
     * @ORM\OneToMany(targetEntity="\AppBundle\Entity\PlanogramCigarette", mappedBy="planogram", cascade={"persist", "refresh", "remove"})
     */
    private $cigarettes;

    public function __construct()
    {
        $this->setCreatedAt(new \DateTime());
    }

    /**
     * @ORM\PrePersist
     * @ORM\PreUpdate
     */
    public function updatedTimestamps()
    {
        $this->setUpdatedAt(new \DateTime('now'));
        if ($this->getCreatedAt() === null) {
            $this->setCreatedAt(new \DateTime('now'));
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param integer $type
     *
     * @return Planogram
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set showzoneType
     *
     * @param integer $showzoneType
     *
     * @return Planogram
     */
    public function setShowzoneType($showzoneType)
    {
        $this->showzoneType = $showzoneType;

        return $this;
    }

    /**
     * Get showzoneType
     *
     * @return int
     */
    public function getShowzoneType()
    {
        return $this->showzoneType;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Planogram
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Planogram
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set user
     *
     * @param \AppBundle\Entity\BaseUser $user
     *
     * @return Planogram
     */
    public function setUser(\AppBundle\Entity\BaseUser $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \AppBundle\Entity\BaseUser
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set keyVisual
     *
     * @param \AppBundle\Entity\KeyVisual $keyVisual
     *
     * @return Planogram
     */
    public function setKeyVisual(\AppBundle\Entity\KeyVisual $keyVisual = null)
    {
        $this->keyVisual = $keyVisual;

        return $this;
    }

    /**
     * Get keyVisual
     *
     * @return \AppBundle\Entity\KeyVisual
     */
    public function getKeyVisual()
    {
        return $this->keyVisual;
    }

    /**
     * Set brand
     *
     * @param \AppBundle\Entity\Brand $brand
     *
     * @return Planogram
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;

        return $this;
    }

    /**
     * Get brand
     *
     * @return \AppBundle\Entity\Brand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * Set vendor
     *
     * @param \AppBundle\Entity\Vendor $vendor
     *
     * @return Planogram
     */
    public function setVendor(\AppBundle\Entity\Vendor $vendor = null)
    {
        $this->vendor = $vendor;

        return $this;
    }

    /**
     * Get vendor
     *
     * @return \AppBundle\Entity\Vendor
     */
    public function getVendor()
    {
        return $this->vendor;
    }

    /**
     * Set stripe
     *
     * @param \AppBundle\Entity\Stripe $stripe
     *
     * @return Planogram
     */
    public function setStripe(\AppBundle\Entity\Stripe $stripe = null)
    {
        $this->stripe = $stripe;

        return $this;
    }

    /**
     * Get stripe
     *
     * @return \AppBundle\Entity\Stripe
     */
    public function getStripe()
    {
        return $this->stripe;
    }

    /**
     * Set showzone
     *
     * @param \AppBundle\Entity\Showzone $showzone
     *
     * @return Planogram
     */
    public function setShowzone(\AppBundle\Entity\Showzone $showzone = null)
    {
        $this->showzone = $showzone;

        return $this;
    }

    /**
     * Get showzone
     *
     * @return \AppBundle\Entity\Showzone
     */
    public function getShowzone()
    {
        return $this->showzone;
    }

    /**
     * Set sort
     *
     * @param \AppBundle\Entity\Sort $sort
     *
     * @return Planogram
     */
    public function setSort(\AppBundle\Entity\Sort $sort = null)
    {
        $this->sort = $sort;

        return $this;
    }

    /**
     * Get sort
     *
     * @return \AppBundle\Entity\Sort
     */
    public function getSort()
    {
        return $this->sort;
    }

    /**
     * Add cigarette
     *
     * @param \AppBundle\Entity\PlanogramCigarette $cigarette
     *
     * @return Planogram
     */
    public function addCigarette(\AppBundle\Entity\PlanogramCigarette $cigarette)
    {
        $cigarette->setPlanogram($this);
        $this->cigarettes[] = $cigarette;

        return $this;
    }

    /**
     * Remove cigarette
     *
     * @param \AppBundle\Entity\PlanogramCigarette $cigarette
     */
    public function removeCigarette(\AppBundle\Entity\PlanogramCigarette $cigarette)
    {
        $this->cigarettes->removeElement($cigarette);
    }

    /**
     * Get cigarettes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCigarettes()
    {
        return $this->cigarettes;
    }
}
