<?php
namespace AppBundle\Service;

use AppBundle\Entity\Sort;
use AppBundle\Form\Api\SortType;
use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class SortService extends AbstractService
{
    /**
     * SortService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Sort::class);
        parent::__construct(
            Sort::class,
            SortType::class,
            $this->repository,
            $formFactory, $entityManager,
            $filterProcessor,
            $logger
        );
    }

    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new Sort();
    }
}