<?php

namespace AppBundle\Service;

use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Psr\Log\LoggerInterface;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

abstract class AbstractService
{
    /**
     * Create entity.
     * @return EntityInterface
     */
    abstract protected function createEntity(): EntityInterface;

    /**
     * @var EntityInterface
     */
    protected $entityInstance;

    /**
     * @var string
     */
    protected $entityClassName;

    /**
     * @var string
     */
    protected $formClassName;

    /**
     * @var FormFactoryInterface
     */
    protected $formFactory;

    /**
     * @var EntityRepository
     */
    protected $repository;

    /**
     * @var EntityManagerInterface
     */
    protected $entityManager;

    /**
     * @var FilterProcessor
     */
    protected $filterProcessor;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var FormInterface
     */
    protected $form;

    /**
     * AbstractService constructor.
     *
     * @param string $entityClassName
     * @param string $formClassName
     * @param EntityRepository $repository
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        string $entityClassName,
        string $formClassName,
        EntityRepository $repository,
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityClassName = $entityClassName;
        $this->formClassName = $formClassName;
        $this->repository = $repository;
        $this->formFactory = $formFactory;
        $this->entityManager = $entityManager;
        $this->filterProcessor = $filterProcessor;
        $this->logger = $logger;
    }

	/**
     * @return array
     */
    public function findAll(): array
    {
        return $this->repository->findAll();
    }
	
	/**
	 * @param $id
	 * @return null|object
	 */
    public function find($id)
    {
        try {
            $entity = $this->repository->find($id);
        } catch (\Exception $exception) {
	        $this->logger->error($exception);
	        $entity = null;
        }

        return $entity;
    }

    /**
     * @param array $filters
     * @param string $sort
     * @param string $sortField
     * @param int $limit
     * @param int $offset
     *
     * @return array|null
     */
    public function findBy(
        array $filters = [],
        string $sort = null,
        string $sortField = null,
        int $limit = null,
        int $offset = null
    ): ?array
    {
        $sort = $sort ? $sort : 'desc';
        $sortField = $sortField ? $sortField : 'id';
        $limit = $limit ? $limit : 20;
        $offset = $offset ? $offset : 0;

        //filter query
        $this->filterProcessor->makeQuery(
            $filters, $this->entityClassName, $sortField, $sort, $limit, $offset
        );

        return $this->filterProcessor->getResults();
    }

	/**
	 * @param Request $request
	 * @return EntityInterface|null
	 */
    public function create(Request $request): ?EntityInterface
    {
        $this->entityInstance = $this->createEntity();
        $this->form = $this->formFactory->create($this->formClassName, $this->entityInstance);
        $this->form->submit($request->request->all());

        return $this->save();
    }

    /**
     * @param Request $request
     * @return EntityInterface|null
     */
    public function update($id, Request $request): ?EntityInterface
    {
        $this->entityInstance = $this->repository->find($id);
        if($this->entityInstance){
            $this->form = $this->formFactory->create($this->formClassName, $this->entityInstance, array(
                "method" => "PUT"
            ));
            $this->form->submit($request->request->all(), false);

            return $this->save();
        }

        return null;
    }

    /**
     * @return EntityInterface|null
     */
    protected function save(): ?EntityInterface
    {
        if ($this->form->isSubmitted() && $this->form->isValid()) {
            //persist and send record to database
            $this->entityManager->persist($this->entityInstance);
            $this->entityManager->flush();
            return $this->entityInstance;
        }

        return null;
    }

    /**
     * @return FormInterface|null
     */
    public function getForm(): ?FormInterface
    {
        return $this->form;
    }

    /**
     * @param FormInterface $form
     */
    public function setForm(FormInterface $form): void
    {
        $this->form = $form;
    }

}
