<?php
namespace AppBundle\Service;

use AppBundle\Form\Api\ShowzoneType;
use AppBundle\Entity\Showzone;
use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class ShowzoneService extends AbstractService
{
    /**
     * ShowzoneService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Showzone::class);
        parent::__construct(
            Showzone::class,
            ShowzoneType::class,
            $this->repository,
            $formFactory, $entityManager,
            $filterProcessor,
            $logger
        );
    }

    /**
     * @return array|null
     */
    public function getShowzoneTypes(): ?array
    {
        //swapping key values are better for frontend understanding
        return array_flip(Showzone::$TYPES);
    }
    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new Showzone();
    }
}