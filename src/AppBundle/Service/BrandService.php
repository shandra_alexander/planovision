<?php
namespace AppBundle\Service;

use AppBundle\Entity\Brand;
use AppBundle\Entity\EntityInterface;
use AppBundle\Form\Api\BrandType;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormFactoryInterface;

class BrandService extends AbstractService
{
    /**
     * BrandService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Brand::class);
        parent::__construct(Brand::class, BrandType::class, $this->repository, $formFactory, $entityManager, $filterProcessor, $logger);
    }

    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new Brand();
    }
}