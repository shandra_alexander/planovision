<?php
namespace AppBundle\Service;

use AppBundle\Form\Api\VendorType;
use AppBundle\Entity\Vendor;
use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class VendorService extends AbstractService
{
    /**
     * VendorService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Vendor::class);
        parent::__construct(
            Vendor::class,
            VendorType::class,
            $this->repository,
            $formFactory, $entityManager,
            $filterProcessor,
            $logger
        );
    }

    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new Vendor();
    }
}