<?php
namespace AppBundle\Service;

use AppBundle\Form\Api\CigaretteType;
use AppBundle\Entity\Cigarette;
use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class CigaretteService extends AbstractService
{
    /**
     * CigaretteService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Cigarette::class);
        parent::__construct(
            Cigarette::class,
            CigaretteType::class,
            $this->repository,
            $formFactory, $entityManager,
            $filterProcessor,
            $logger
        );
    }

    /**
     * @return array|null
     */
    public function getCigaretteTypes(): ?array
    {
        //swapping key values are better for frontend understanding
        return array_flip(Cigarette::$TYPES);
    }

    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new Cigarette();
    }
}