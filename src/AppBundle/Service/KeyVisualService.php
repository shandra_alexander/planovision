<?php
namespace AppBundle\Service;

use AppBundle\Form\Api\KeyVisualType;
use AppBundle\Entity\KeyVisual;
use AppBundle\Entity\EntityInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;

class KeyVisualService extends AbstractService
{
    /**
     * KeyVisualService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(KeyVisual::class);
        parent::__construct(
            KeyVisual::class,
            KeyVisualType::class,
            $this->repository,
            $formFactory, $entityManager,
            $filterProcessor,
            $logger
        );
    }

    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new KeyVisual();
    }
}