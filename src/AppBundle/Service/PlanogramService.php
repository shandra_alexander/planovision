<?php
namespace AppBundle\Service;

use AppBundle\Form\Api\PlanogramType;
use AppBundle\Entity\Planogram;
use AppBundle\Entity\EntityInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Form\FormFactoryInterface;
use Symfony\Component\HttpFoundation\Request;

class PlanogramService extends AbstractService
{
    /** @var Planogram $entityInstance */
    protected $entityInstance;

    /** @var ArrayCollection $originalCigarettes */
    private $originalCigarettes;

    /**
     * PlanogramService constructor.
     * @param FormFactoryInterface $formFactory
     * @param EntityManagerInterface $entityManager
     * @param FilterProcessor $filterProcessor
     * @param LoggerInterface $logger
     */
    public function __construct(
        FormFactoryInterface $formFactory,
        EntityManagerInterface $entityManager,
        FilterProcessor $filterProcessor,
        LoggerInterface $logger
    ) {
        $this->entityManager = $entityManager;
        $this->repository = $this->entityManager->getRepository(Planogram::class);
        parent::__construct(
            Planogram::class,
            PlanogramType::class,
            $this->repository,
            $formFactory,
            $entityManager,
            $filterProcessor,
            $logger
        );
    }

    /**
     * @param $id
     * @param Request $request
     * @return EntityInterface|null
     */
    public function update($id, Request $request): ?EntityInterface
    {
        $this->entityInstance = $this->repository->find($id);

        $this->originalCigarettes = new ArrayCollection();
        foreach ($this->entityInstance->getCigarettes() as $cigarette){
            $this->originalCigarettes->add($cigarette);
        }

        if($this->entityInstance){
            $this->form = $this->formFactory->create($this->formClassName, $this->entityInstance, array(
                "method" => "PUT"
            ));
            $this->form->submit($request->request->all(), false);

            return $this->save();
        }
    }

    /**
     * @return EntityInterface|null
     */
    public function save(): ?EntityInterface
    {
        if ($this->form->isSubmitted() && $this->form->isValid()) {

            foreach ($this->originalCigarettes as $planogramCigarette) {
                if (false === $this->entityInstance->getCigarettes()->contains($planogramCigarette)) {
                    $this->entityManager->remove($planogramCigarette);
                }
            }

            $this->entityManager->persist($this->entityInstance);
            $this->entityManager->flush();
            return $this->entityInstance;
        }
    }

    /**
     * @return array|null
     */
    public function getPlanogramTypes(): ?array
    {
        return array_flip(Planogram::$TYPES);
    }

    /**
     * @return array|null
     */
    public function getPlanogramShowzoneTypes(): ?array
    {
        return array_flip(Planogram::$SHOWZONE_TYPES);
    }

    /**
     * Create entity.
     * @return EntityInterface
     */
    protected function createEntity(): EntityInterface
    {
        return new Planogram();
    }
}