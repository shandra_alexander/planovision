planovision
===========

Начало Работы
--------------

Для начала склонируйте проект на локальную машину. Первым делом необходимо установить все зависимости проекта. Вам нужно удостовериться что у вас установлен [Composer](https://getcomposer.org/download/) для того чтобы вы могли произвести установку зависимостей:
```
git clone https://shandra_alexander@bitbucket.org/shandra_alexander/planovision.git
cd planovision
composer install
```
Не забудьте указать правильные параметры в конце устанвоки, такие как database_name, database_user, database_password...

Установка Сервера
----------------

### PHP Built-in Server

Произведите установку сервера в зависимости от сервера вашего выбора. Проект также имеет в себе встроенный PHP сервер, который можно запустить следующим путем:
```
cd planovision
php bin/console server:run

### or if you prefer to specify port and ip: php bin/console server:start 127.0.0.1:8000
```

### NGINX

Если вы хотите установить проект на NGINX, посмотрите на следующие примеры конфигурации сервера NGINX с Symfony:

*  [Symfony Documentation: Web Server Configuration](https://symfony.com/doc/3.4/setup/web_server_configuration.html#nginx)
*  [NGINX Documentation: Symfony](https://www.nginx.com/resources/wiki/start/topics/recipes/symfony/#secure-symfony-3-x-2-x)

Не забудьте правильно указать местонахождение проекта и название сервера в конфигурации, а также добавить название сайта в /etc/hosts файл.

Далее вам необходимо сделать папки var открытыми для записи. Это можно сделать следующим образом:
```
cd planovision
HTTPDUSER=$(ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1)
sudo setfacl -dR -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
sudo setfacl -R -m u:"$HTTPDUSER":rwX -m u:$(whoami):rwX var
```
Более подробную информации о правах записи, чтения и исполнения, вы можете узнать здесь: [Symfony: Setting up or Fixing File Permissions](https://symfony.com/doc/3.4/setup/file_permissions.html)

Установка Базы Данных
--------------------

Для данного проекта использовалась база [MySQL 5.7.24](https://dev.mysql.com/downloads/mysql/5.7.html). 
Проследуйте инструкциям установки на сайте разработчика если у вас нет данной базы данных.

Далее вам необходимо отрегулировать базу данных.
```
php bin/console doctrine:database:create
php bin/console doctrine:schema:update --force
```
На данном этапе у нас есть база с правильной схемой.
Заполнить фэйковыми данными ее через слудующую.
```
php bin/console doctrine:fixtures:load
```
Чтобы запустить проект, используйте эту команду
```
php bin/console server:start
```
Для запуска приложения можно воспользоваться командой. Для редактирования данных можно зайти в админ панель по адресу
http://127.0.0.1:8000/dashboard. В фикстурах указан логин и пароль администратора
```
nano src/AppBundle/DataFixtures/AppFixtures.php
```

Завершение Установки
-------------------

Не забудьте почистить кэш перед запуском:
```
php bin/console cache:clear
php bin/console cache:clear -e prod
```

На данном этапе вы готовы открыть проект: http://127.0.0.1:8000/


Навигация по проекту
--------------
* Controllers => *project*/src/AppBundle/Controller
* Fixtures => *project*/src/AppBundle/Datauction-beeline-back-endaFixtures
* Entities => *project*/src/AppBundle/Entities
* Services => *project*/src/AppBundle/Services
Устранение Проблем
------------------

Если у вас не получилось произвести установку, проверьте если все требования для Symfony были соблюдены.
Все необходимые требования для различных компонентов вы сможете найти в списке ниже: 

* [Composer Requirements](https://getcomposer.org/doc/00-intro.md#system-requirements)
* [Symfony Requirements](https://symfony.com/doc/3.4/reference/requirements.html)
* [NGINX Requirements](https://www.nginx.com/resources/wiki/start/topics/tutorials/gettingstarted/#requirements)

Как указано в [Symfony Requirements](https://symfony.com/doc/3.4/reference/requirements.html), вы всегда можете проверить если у вас есть недостатки в системе необходимых зависимостей:
```
cd planovision
php bin/symfony_requirements
```
После соблюдения всех условий, попробуйте установить проект снова.